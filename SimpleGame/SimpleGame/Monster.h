#pragma once
#include "Object.h"

class CObject;

class CMonster :
	public CObject
{
private:
	CObject*		m_pPlayer;
	MONSTER_TYPE	m_eMonsterType;
	BOSS_STATUS		m_eBossStatus;

	Vec3			m_vPrevPos;
	Vec3			m_vDir;
	Vec3			m_vDist;
	Vec3			m_vPlayerPos;

	bool			m_bShootBullet;
	float			m_fJumpCoolTime;
	float			m_fDirTimer;
	int				m_iDir;

	bool			m_bLastPattern;
	bool			m_bAnimChange;

public:
	virtual int update();
	virtual void init();

public:
	void SetMonsterType(MONSTER_TYPE _eType) { m_eMonsterType = _eType; }
	const MONSTER_TYPE& GetMonsterType() { return m_eMonsterType; }

	void SetPlayer(CObject* pObject) { m_pPlayer = pObject; }
	const CObject* GetPlayer() { return m_pPlayer; }

	void SetPlayerPos(Vec3 vPlayerPos) { m_vPlayerPos = vPlayerPos; }
	const Vec3& GetPlayerPos() { return m_vPlayerPos; }

	const Vec3& GetMonsterDir() { return m_vDir; }
	
	void SetBossStatus(BOSS_STATUS eStatus) { m_eBossStatus = eStatus; }
	const BOSS_STATUS& GetBossStatus() { return m_eBossStatus; }

	void SetShootBullet(bool _bool) { m_bShootBullet = _bool; }
	const bool& GetShootBullet() { return m_bShootBullet; }

	void SetMonsterDir(int iDir) { m_iDir = iDir; }
	const int& GetDir() { return m_iDir; }

	void SetAnimChange(bool _b) { m_bAnimChange = _b; }
	const bool& GetAnimChange() { return m_bAnimChange; }

	void BossPattern();
	void ActionPattern(BOSS_STATUS eStatus);
	void Status();
	void Animation();
	void ChangeMonster();


public:
	void BossMove();
	void NormalMove();
	void BossAttack();
	void MonsterAttack();

public:
	bool CanShootBullet();
	void ResetBulletCoolTime();

public:
	virtual void AddForce(Vec3 vForce, float fTime);

public:
	CMonster();
	virtual ~CMonster();
};

