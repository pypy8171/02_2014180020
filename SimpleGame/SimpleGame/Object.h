#pragma once
#include "global.h"

class CObject
{
protected:
	OBJ_TYPE	m_eObjType;
	LOOK_DIR	m_eLookDir;
	CObject*	m_pOwner;

	Vec3		m_vPos;
	Vec3		m_vSize;
	Vec4		m_vColor;

	Vec3		m_vVel;
	Vec3		m_vAcc;
	Vec3		m_vVol;
	float		m_fMass;
	float		m_fAccTime;
	float		m_fFricCoef;

	float		m_fForce;

	float		m_fHp;
	float		m_fOffensePower;
	float		m_fBulletCoolTime;
	float		m_fRemainingBulletCoolTime;

	int			m_iTextureID;

	Vec2		m_iAnimIdx;
	float		m_fAnimDuration;
	bool		m_bAnimEnd;

	float		m_fInvinDT;
	float		m_fBeforeInvinDT;
	OBJ_STATUS	m_eObjStatus;
	bool		m_bJump;


public:
	virtual int update();
	virtual void init() = 0;

public:
	void SetObjType(OBJ_TYPE objtype) {m_eObjType = objtype;}
	void SetLookDir(LOOK_DIR eDir) { m_eLookDir = eDir; }
	void SetObjStatus(OBJ_STATUS objstatus) { m_eObjStatus = objstatus; }

	const OBJ_TYPE& GetObjType() { return m_eObjType; }
	const LOOK_DIR& GetLookDir() { return m_eLookDir; }
	const OBJ_STATUS& GetObjStatus() { return m_eObjStatus; }

public:
	void SetObjPos(Vec3 vPos) {m_vPos = vPos;}
	void SetObjSize(Vec3 vSize) { m_vSize = vSize; }
	void SetObjVel(Vec3 vVel) {	m_vVel = vVel;}
	void SetObjAcc(Vec3 vAcc) { m_vAcc = vAcc; }
	void SetObjVol(Vec3 vVol) { m_vVol = vVol; }
	void SetObjColor(Vec4 vColor){ m_vColor = vColor; }
	void SetObjMass(float vMass) { m_fMass = vMass; }
	void SetObjFricCoef(float fFricCoef) { m_fFricCoef = fFricCoef; }
	void SetObjHp(float fHp) { m_fHp = fHp; }

	const Vec3& GetObjPos() { return m_vPos; }
	const Vec3& GetObjSize() { return m_vSize; }
	const Vec3& GetObjVel() { return m_vVel; }
	const Vec3& GetObjAcc() { return m_vAcc; }
	const Vec3& GetObjVol() { return m_vVol; }
	const Vec4& GetObjColor() { return m_vColor; }
	const float& GetObjMass() { return m_fMass; }
	const float& GetObjFricCoef() { return m_fFricCoef; }
	const float& GetObjHp() { return m_fHp; }

	void SetHp(float fDamage) { m_fHp -= fDamage; }
	void SetOffensePower(float fPower) { m_fOffensePower = fPower; }
	void SetBulletCoolTime(float fCoolTime) { m_fBulletCoolTime = fCoolTime; }
	void SetRemainingBulletCoolTime(float fRCoolTime) { m_fRemainingBulletCoolTime = fRCoolTime; }

	const float& GetHp() { return m_fHp; }
	const float& GetOffensePower() { return m_fOffensePower; }
	const float& GetBulletCoolTime() { return m_fBulletCoolTime; }
	const float& GetRemainingBulletCoolTime() { return m_fRemainingBulletCoolTime; }

	void SetTextureID(int ID) { m_iTextureID = ID; }
	const int& GetTextureID() { return m_iTextureID; }

	void SetOwner(CObject* pObject) { m_pOwner = pObject; }
	CObject* GetOwner() { return m_pOwner; }

	bool IsAncestor(CObject* obj);
	const bool& Jump() { return m_bJump; }

public:
	void SetAnimIdx(Vec2 _idx) { m_iAnimIdx = _idx; }
	const Vec2& GetAnimIdx() { return m_iAnimIdx; }

	void SetAnimEnd(bool _b) { m_bAnimEnd = _b; }
	const bool& GetAnimEnd() { return m_bAnimEnd; }

public:
	virtual void Addforce(Vec3 vForce, float fTime) {};
	virtual void SubForce(Vec3 vForce, float fTime) {};

	virtual bool CanShootBullet() { return 0; }
	virtual void Status() {};
	virtual void ResetBulletCoolTime() {};

public:
	void SetForce(float _fForce) { m_fForce = _fForce; }
	const float& GetForce() { return m_fForce; }

public:
	void AddGameObj(Vec3 vPos, Vec3 vSize, Vec4 vColor);
	void DeleteGameObj(int iIdx);

public:
	CObject();
	virtual ~CObject();
};

