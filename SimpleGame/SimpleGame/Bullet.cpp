#include "stdafx.h"
#include "Bullet.h"

#include "ScnMgr.h"
#include "TimeMgr.h"
#include "Sound.h"

#include "Player.h"
#include "Monster.h"

// 바라보고 있는 방향으로 총알 나가게 하도록

CBullet::CBullet()
	: m_pPlayer(nullptr)
	, m_bKnowOwner(false)
	, m_vDir(Vec3(0.f,0.f,0.f))
	, m_bShooted(false)
	, m_iBounceCount(0)
	
{
	init();
}


CBullet::~CBullet()
{
	//SAFE_DELETE(m_pPlayer);
}

void CBullet::init()
{
	SetObjType(OBJ_TYPE::BULLET);
	SetBulletType(BULLET_TYPE::NONE);
	m_vVel = Vec3(0.f, 0.f, 0.f);
	SetOffensePower(10.f);
	SetObjHp(1.f);
}

int CBullet::update()
{
	CObject::update();
	Shoot();

	m_fAnimDuration += DT;

	if (m_fAnimDuration > 0.1f && GetCollided())
	{
		if (m_iAnimIdx.x == 0 && GetOwner()->GetObjType() == OBJ_TYPE::PLAYER)
		{
			int sound = CScnMgr::GetInst()->GetSound(0);
			Sound::GetInst()->PlayShortSound(sound, false, 1);
		}

		m_vVel = Vec3(0.f, 0.f, 0.f);
		m_iAnimIdx.x += 1;
		if (m_iAnimIdx.x > 15)
		{
			SetAnimEnd(true);
			m_iAnimIdx.x = 15;

		}
		m_fAnimDuration = 0.f;
	}

	return 0;
}

void CBullet::AddForce(Vec3 vForce, float fTime)
{
	Vec3 vAcc = vForce / m_fMass;

	if(OBJ_TYPE::PLAYER == m_pOwner->GetObjType())
		m_vVel = ((CPlayer*)m_pOwner)->GetObjVel();
	else if (OBJ_TYPE::MONSTER == m_pOwner->GetObjType())
	{
		if (m_vVel.x == 0.f)
		{
			m_vVel = m_vDir * 4.f;
		}
	}
	else if (OBJ_TYPE::BOSS == m_pOwner->GetObjType())
	{

	}

	m_vVel = m_vVel + vAcc*DT; // fTime 대신 DT 넣자
}

void CBullet::Shoot()
{
	Vec3 vForce = Vec3(0.f, 0.f, 0.f);

	Vec3 vPos = Vec3(0.f, 0.f, 0.f);
	LOOK_DIR vDir;

	if (nullptr == m_pOwner)
		return;

	if (!m_bKnowOwner)
	{
		vPos = m_pOwner->GetObjPos();
		vDir = m_pOwner->GetLookDir();

		this->SetObjPos(vPos);
		this->SetLookDir(vDir);

		m_bKnowOwner = true;
		//return;
	}

	vPos = this->GetObjPos();
	vDir = this->GetLookDir();

	if (m_vPos.x < -2.f || m_vPos.x > 2.f || m_vPos.y < -2.0f || m_vPos.y > 1.2f)
	{ 
		SetCollided(true); 
	}


	if (OBJ_TYPE::PLAYER == m_pOwner->GetObjType())
	{
		float fAmount = GetForce();

		if (LOOK_DIR::RIGHT == vDir){vForce.x += fAmount;}
		else if (LOOK_DIR::LEFT == vDir){vForce.x -= fAmount;}
		else if (LOOK_DIR::UP == vDir){vForce.y += fAmount;}
		else if (LOOK_DIR::DOWN == vDir){vForce.y-= fAmount;}

		if (!GetBulletShooted())
		{
			AddForce(vForce, DT);
			SetBulletShooted(true);
		}
		SetObjPos(Vec3(m_vPos.x + m_vVel.x*DT, m_vPos.y + m_vVel.y*DT, m_vPos.z));
	}
	else if (OBJ_TYPE::MONSTER == m_pOwner->GetObjType())
	{
 		float fAmount = GetForce();

		if (BULLET_TYPE::NORMAL == this->GetBulletType()) // 안움직이는 미사일은 이거일듯
		{
			if (m_vDir == (Vec3(0.f, 0.f, 0.f)))
				m_vDir = ((CMonster*)m_pOwner)->GetMonsterDir();
		}
		else if (BULLET_TYPE::GUIDED == this->GetBulletType() && m_pPlayer != nullptr)
		{
			Vec3 vBulletPos = GetObjPos();
			Vec3 vPlayerPos = m_pPlayer->GetObjPos();

			m_vDir = vPlayerPos - vBulletPos;
			m_vDir.Normalize();
		}
		else if (BULLET_TYPE::TARGETED == this->GetBulletType() && !(this->GetBulletShooted()) && m_pPlayer != nullptr)
		{
			Vec3 vBulletPos = GetObjPos();
			Vec3 vPlayerPos = m_pPlayer->GetObjPos();

			m_vDir = vPlayerPos - vBulletPos;
			m_vDir.Normalize();

			//SetBulletShooted(true);
		}
		if (!GetBulletShooted())
		{
			vForce.x += m_vDir.x * fAmount;
			vForce.y += m_vDir.y * fAmount;
			AddForce(vForce, DT);
			SetBulletShooted(true);
		}
		SetObjPos(Vec3(vPos.x + m_vVel.x*DT * 0.1f, vPos.y +  m_vVel.y*DT*0.1f, vPos.z));
	}
	else if (OBJ_TYPE::BOSS == m_pOwner->GetObjType())
	{

		float fAmount = GetForce();

		if (BULLET_TYPE::MULTI == this->GetBulletType()) // 안움직이는 미사일은 이거일듯
		{
			CWell512 random;

			m_vDir.x = random.GetFloatValue(-1.f, 1.f);
			m_vDir.y = random.GetFloatValue(-0.5f, 1.f);
			//m_vDir.z = random.GetFloatValue(-1.f, 1.f);

			m_vDir.Normalize();
		}

		if (!GetBulletShooted())
		{
			vForce.x += m_vDir.x * fAmount;
			vForce.y += m_vDir.y * fAmount;
			//vForce.z += m_vDir.z * fAmount;
			AddForce(vForce, DT);
			SetBulletShooted(true);
		}

		SetObjPos(Vec3(vPos.x + m_vVel.x*DT, vPos.y + m_vVel.y*DT, vPos.z + m_vVel.y*DT));
	}
}