#include "stdafx.h"
#include "Physics.h"

#include "Object.h"

CPhysics::CPhysics()
{
}


CPhysics::~CPhysics()
{
}

bool CPhysics::IsCollision(CObject * pFirstObject, CObject * pSecondObject, int iType)
{
	switch (iType)
	{
	case 0:
		return BBcollision(pFirstObject, pSecondObject);
		break;
	case 1:
		break;
	}

	return false;
}

void CPhysics::ProcessCollision(CObject * pFirstObject, CObject * pSecondObject)
{
	float fMassFirst = pFirstObject->GetObjMass();
	Vec3 vVelocityFirst = pFirstObject->GetObjVel();

	float fMassSecond = pSecondObject->GetObjMass();
	Vec3 vVelocitySecond = pSecondObject->GetObjVel();

	Vec3 vFirstVel, vSecondVel;
	vFirstVel.x = ((fMassFirst - fMassSecond) / (fMassFirst + fMassSecond))*vVelocityFirst.x + 2 * ((fMassSecond) / (fMassFirst + fMassSecond))*vVelocitySecond.x;
	vFirstVel.y = ((fMassFirst - fMassSecond) / (fMassFirst + fMassSecond))*vVelocityFirst.y + 2 * ((fMassSecond) / (fMassFirst + fMassSecond))*vVelocitySecond.y;

	vSecondVel.x = (2*(fMassFirst) / (fMassFirst + fMassSecond))*vVelocityFirst.x + ((fMassSecond - fMassFirst) / (fMassFirst + fMassSecond))*vVelocitySecond.x;
	vSecondVel.y = (2*(fMassFirst) / (fMassFirst + fMassSecond))*vVelocityFirst.y + ((fMassSecond - fMassFirst) / (fMassFirst + fMassSecond))*vVelocitySecond.y;

	// z�� ����
	pFirstObject->SetObjVel(vFirstVel*0.25f);
	pSecondObject->SetObjVel(vSecondVel*0.25f);
}

bool CPhysics::BBcollision(CObject * pFirstObject, CObject * pSecondObject)
{

	Vec3 vFirstSize = pFirstObject->GetObjSize();
	Vec3 vFirstPos = pFirstObject->GetObjPos();

	Vec3 vSecondSize = pSecondObject->GetObjSize();
	Vec3 vSecondPos = pSecondObject->GetObjPos();

	float fFirstMinX = vFirstPos.x - vFirstSize.x * 0.5f;
	float fFirstMaxX = vFirstPos.x + vFirstSize.x * 0.5f;

	float fFirstMinY = vFirstPos.y - vFirstSize.y * 0.5f;
	float fFirstMaxY = vFirstPos.y + vFirstSize.y * 0.5f;

	float fSecondMinX = vSecondPos.x - vSecondSize.x * 0.5f;
	float fSecondMaxX = vSecondPos.x + vSecondSize.x * 0.5f;

	float fSecondMinY = vSecondPos.y - vSecondSize.y * 0.5f;
	float fSecondMaxY = vSecondPos.y + vSecondSize.y * 0.5f;

	if (fFirstMinX > fSecondMaxX)
		return false;
	if (fFirstMaxX < fSecondMinX)
		return false;

	if (fFirstMinY > fSecondMaxY)
		return false;
	if (fFirstMaxY < fSecondMinY)
		return false;

	return true;
}