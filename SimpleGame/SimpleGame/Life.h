#pragma once
#include "Object.h"
class CLife :
	public CObject
{
private:
	bool		m_bAlive;
	int			m_iLifeCount;

public:
	virtual int update();
	virtual void init();

public:
	void SetLifeCount(int _iCount) { m_iLifeCount -= _iCount; }
	const int& GetLifeCount() { return m_iLifeCount; }

public:
	void SetAlive(bool _bool) { m_bAlive = _bool; }
	const bool& GetAlive() { return m_bAlive; }

public:
	CLife();
	virtual ~CLife();
};

