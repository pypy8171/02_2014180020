#include "stdafx.h"
#include "Life.h"

#include "Renderer.h"
#include "ScnMgr.h"

CLife::CLife()
	: m_iLifeCount(MAX_LIFE)
	, m_bAlive(true)
{
}


CLife::~CLife()
{
}

int CLife::update()
{
	Vec3 vObjPos = this->GetObjPos();
	Vec3 vObjSize = this->GetObjSize();
	Vec4 vObjColor = this->GetObjColor();

	return 0;
}

void CLife::init()
{
}