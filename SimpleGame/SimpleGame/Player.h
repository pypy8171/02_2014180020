#pragma once
#include "Object.h"
class CPlayer :
	public CObject
{
private:
	bool			m_bBulletFull;

	Vec3			m_vPrevPos;
	Vec3			m_vDir;

	int				m_iLife;

	Vec2			m_vBodyIdx;
	Vec2			m_vHeadIdx;
	bool			m_bHeadChange;

	int				m_iMoveAnim;

public:
	virtual int update();
	virtual void init();

public:
	void InputKey();
	void DirKey();
	void Status();

public:
	void SetBulletFull(bool _bool) { m_bBulletFull = _bool; }
	const bool& GetBulletFull() { return m_bBulletFull; }

	void SetLife(int iLife) { m_iLife = iLife; }
	void SetMinusLife() { if (m_iLife > 0) { m_iLife -= 1; } }
	const int& GetLife() { return m_iLife; }

public:
	void SetPlayerMoveAnim(int _iIdx) { m_iMoveAnim = _iIdx; }
	const int& GetPlayerMoveAnim() { return m_iMoveAnim; }


public:
	void SetHeadIdx(Vec2 _iIdx) { m_vHeadIdx = _iIdx; }
	void SetBodyIdx(Vec2 _iIdx) { m_vBodyIdx = _iIdx; }

	const Vec2& GetHeadIdx() { return m_vHeadIdx; }
	const Vec2& GetBodyIdx() { return m_vBodyIdx; }

	//const CObject& GetPlayerLife() { return m_pLife[0]; }
public:
	bool CanShootBullet();
	void ResetBulletCoolTime();

public:
	virtual void AddForce(Vec3 vForce, float fTime);

public:
	CPlayer();
	virtual ~CPlayer();
};

