#include "stdafx.h"
#include "ScnMgr.h"


#include "Renderer.h"
#include "Object.h"
#include "Player.h"
#include "Bullet.h"
#include "Monster.h"
#include "Life.h"
#include "Map.h"
#include "Door.h"

#include "Physics.h"
#include "KeyMgr.h"
#include "TimeMgr.h"
#include "Sound.h"
#include <iostream>

#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"

// CScnMgr* g_scnmgr = nullptr;
// 오브젝트만 만드는 클레스를 따로 싱글톤으로 만드는거 고려해보기
CWell512 randoms;

int g_isound = -1;

CScnMgr::CScnMgr()
	: m_pPlayer(nullptr)
	, m_pRenderer(nullptr)
	, m_pMap(nullptr)
	, m_pLogo(nullptr)
	, m_pSound(nullptr)
	, m_bParticle(false)
	, m_bExit(false)
	, m_eCurMap(MAP::START)
	, m_eBeforeMap(MAP::START)
	, m_pUI(nullptr)
	, m_fResetTimer(0.f)
	, m_bLogo(false)
	, m_bGameStart(true)
	, m_bGameEnd(false)
	, m_bResetOn(false)
	, m_fShakingTimer(0.f)
	, m_bCameraShake(false)
	, m_vCamPos(Vec2(0.f,0.f))
{
	init();
}

// 현재 소멸자 호출 안됨 -> 정상적인 종료 아님. // 해결 콜백함수개념으로 함수 하나 만들어서(전역함수) 호출하면 그에 해당하는 
// 타입의 클래스를 생성(복사생성자 호출함) -> delete 호출 하는 방법으로 소멸자 호출함.
CScnMgr::~CScnMgr()
{
	//DoGarbageCollection();
	DeleteScene();
}

void CScnMgr::init()
{
	// Initialize Renderer
	m_pRenderer = new Renderer(1000, 1000);
	if (!m_pRenderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	m_pSound = new Sound();

	g_isound = Sound::GetInst()->CreateBGSound("../Resource/Sound/BGM1.mp3");
	

	for (int i = 0; i < MAX_PARTICLE; ++i)
	{
		m_iParticleTexture[i] = -1;
		m_iParticleObj[i] = -1;
		m_fParticleDuration[i] = 0.f;
		m_bParticleOn[i] = false;
		m_vParticlePos[i] = (Vec3(0.f, 0.f, 0.f));
	}

	for (int i = 0; i < MAX_DOOR; ++i)
	{
		m_pDoor[i] = NULL;
	}

	for (int i = 0; i < MAX_BULLETS; ++i)
	{
		m_pBullet[i] = NULL;
	}

	for (int i = 0; i < MAX_MONSTERS; ++i)
	{
		m_pMonster[i] = NULL;
	}

	for (int i = 0; i < MAX_LIFE; ++i)
	{
		m_pLife[i] = NULL;
	}

	m_pUI = NULL;
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
	for (int i = 0; i < 30; ++i)
	{
		m_iSound[i] = NULL;
	}

	for (int i = 0; i < 30; ++i)
	{
		m_iTexture[i] = NULL;
	}

	m_iSound[0] = Sound::GetInst()->CreateShortSound("../Resource/Sound/TearsBreak.mp3");
	m_iSound[1] = Sound::GetInst()->CreateShortSound("../Resource/Sound/GhostDeath.mp3");
	m_iSound[2] = Sound::GetInst()->CreateShortSound("../Resource/Sound/IssacHurt.mp3");
	m_iSound[3] = Sound::GetInst()->CreateShortSound("../Resource/Sound/MonsterDeath.mp3");
	m_iSound[4] = Sound::GetInst()->CreateShortSound("../Resource/Sound/BossDeath.mp3");
	m_iSound[5] = Sound::GetInst()->CreateShortSound("../Resource/Sound/Plop.mp3");
	m_iSound[6] = Sound::GetInst()->CreateShortSound("../Resource/Sound/DoorOpen.mp3");
	m_iSound[7] = Sound::GetInst()->CreateShortSound("../Resource/Sound/DoorClose.mp3");
	m_iSound[8] = Sound::GetInst()->CreateShortSound("../Resource/Sound/BossJump.mp3");



	AddObject(Vec3(0.f, 0.f, 0.f), Vec3(1.f, 1.f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 0.f, 0.f, OBJ_TYPE::LOGO);
	AddObject(Vec3(0.f, 0.f, 0.f), Vec3(1.f, 1.f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 0.f, 0.f, OBJ_TYPE::MAP);
	AddObject(Vec3(-1.f, -0.25f, 0.f), Vec3(0.25f, 0.25f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 10.f, 0.3f, OBJ_TYPE::PLAYER);
	
	Sound::GetInst()->PlayBGSound(g_isound, true, 1.0f);//2번째 인자는 루프 3번쨰인자는 볼륨
}

void CScnMgr::initobj()
{
	Sound::GetInst()->DeleteBGSound(g_isound);
	g_isound = Sound::GetInst()->CreateBGSound("../Resource/Sound/BGM1.mp3");
	Sound::GetInst()->PlayBGSound(g_isound, true, 1.0f);//2번째 인자는 루프 3번쨰인자는 볼륨

	for (int i = 0; i < MAX_PARTICLE; ++i)
	{
		m_iParticleTexture[i] = -1;
		m_iParticleObj[i] = -1;
		m_fParticleDuration[i] = 0.f;
		m_bParticleOn[i] = false;
		m_vParticlePos[i] = (Vec3(0.f, 0.f, 0.f));
		m_bParticle = false;
	}

	for (int i = 0; i < MAX_BULLETS; ++i)
	{
		SAFE_DELETE(m_pBullet[i]);
		m_pBullet[i] = nullptr;
	}

	for (int i = 0; i < MAX_MONSTERS; ++i)
	{
		SAFE_DELETE(m_pMonster[i]);
		m_pMonster[i] = nullptr;
	}

	for (int i = 0; i < MAX_LIFE; ++i)
	{
		SAFE_DELETE(m_pLife[i]);
		m_pLife[i] = nullptr;
	}

	for (int i = 0; i < MAX_DOOR; ++i)
	{
		SAFE_DELETE(m_pDoor[i]);
		m_pDoor[i] = nullptr;
	}

	SAFE_DELETE(m_pUI);
	m_pUI = nullptr;

	SAFE_DELETE(m_pMap);
	m_pMap = nullptr;

	SAFE_DELETE(m_pPlayer);
	m_pPlayer = nullptr;

	SAFE_DELETE(m_pLogo);
	m_pLogo = nullptr;

	m_eCurMap = MAP::START;
	m_eBeforeMap = MAP::START;

	m_bLogo = false;
	m_bGameStart = true;
	m_bGameEnd = false;
	m_bResetOn = false;
	
	AddObject(Vec3(0.f, 0.f, 0.f), Vec3(1.f, 1.f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 0.f, 0.f, OBJ_TYPE::LOGO);
	AddObject(Vec3(0.f, 0.f, 0.f), Vec3(1.f, 1.f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 0.f, 0.f, OBJ_TYPE::MAP);
	AddObject(Vec3(-1.f, 0.25f, 0.f), Vec3(0.25f, 0.25f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 10.f, 0.3f, OBJ_TYPE::PLAYER);
}

int CScnMgr::update(float eTime)
{
	static bool init = false;
	if (!init)
	{
		m_iTexture[0] = m_pRenderer->GenPngTexture("../Resource/Texture/PlayerSprite.png");
		m_iTexture[1] = m_pRenderer->GenPngTexture("../Resource/Texture/PlayerTear.png");
		m_iTexture[2] = m_pRenderer->GenPngTexture("../Resource/Texture/BossLeft.png");
		m_iTexture[3] = m_pRenderer->GenPngTexture("../Resource/Texture/Ghost.png");
		m_iTexture[4] = m_pRenderer->GenPngTexture("../Resource/Texture/DevilStone.png");
		m_iTexture[5] = m_pRenderer->GenPngTexture("../Resource/Texture/Start.png");
		m_iTexture[6] = m_pRenderer->GenPngTexture("../Resource/Texture/Room1.png");
		m_iTexture[7] = m_pRenderer->GenPngTexture("../Resource/Texture/BossRoom.png");
		m_iTexture[8] = m_pRenderer->GenPngTexture("../Resource/Texture/Door.png");
		m_iTexture[9] = m_pRenderer->GenPngTexture("../Resource/Texture/Start.png");
		m_iTexture[10] = m_pRenderer->GenPngTexture("../Resource/Texture/UI.png");
		m_iTexture[11] = m_pRenderer->GenPngTexture("../Resource/Texture/EndingE.png");
		m_iTexture[12] = m_pRenderer->GenPngTexture("../Resource/Texture/MainE.png");
		m_iTexture[13] = m_pRenderer->GenPngTexture("../Resource/Texture/Life.png");
		init = true;
	}

	CKeyMgr::GetInst()->update();
	CTimeMgr::GetInst()->update();

	CemeraShaking();

	if (m_bResetOn)
	{
		m_fResetTimer += DT;
		
		if (m_fResetTimer > 3.f)
		{
			m_bGameEnd = true;

			initobj();
			m_fResetTimer = 0.f;
			m_bResetOn = false;
		}
	}

	if (KEYTAB(KEY_TYPE::KEY_ESC))
	{
		m_bExit = true;
		CallBack(CKeyMgr::GetInst());
		CallBack(this);
		return 0;
	}

	if (m_pLogo != nullptr && m_bLogo)
		return 0;

	if (m_pPlayer != nullptr)
		m_pPlayer->update();

	for (int i = 0; i < MAX_BULLETS; ++i)
	{
		if (nullptr != m_pBullet[i])
			m_pBullet[i]->update();
	}

	for (int i = 0; i < MAX_MONSTERS; ++i)
	{
		if (m_pMonster[0] == NULL && MAP::BOSSROOM == m_eCurMap)
		{
			m_bResetOn = true;
			DeleteObject(i, m_pMonster[i]);
			continue;
		}

		if (nullptr != m_pMonster[i])
		{
			m_pMonster[i]->update();
			if (m_pPlayer != nullptr)
				((CMonster*)m_pMonster[i])->SetPlayer(m_pPlayer);
			else
				((CMonster*)m_pMonster[i])->SetPlayer(nullptr);

			if (m_eCurMap == MAP::BOSSROOM)
			{
				if (nullptr != m_pMonster[0])
				{
					if (((CMonster*)m_pMonster[i])->GetObjType() == OBJ_TYPE::MONSTER && !((CMonster*)m_pMonster[i])->GetAnimChange() && ((CMonster*)m_pMonster[i]->GetOwner())->GetBossStatus() == BOSS_STATUS::PATTERN_FOURTH)
					{
						((CMonster*)m_pMonster[i])->SetAnimChange(true);
						m_pMonster[i]->SetTextureID(m_iTexture[2]);
						Vec3 vOwnerSize = m_pMonster[i]->GetOwner()->GetObjSize();
						m_pMonster[i]->SetObjSize(vOwnerSize);

						m_pMonster[i]->SetObjHp(BOSS_HP);
						m_pMonster[i]->SetObjType(OBJ_TYPE::BOSS);
						((CMonster*)m_pMonster[i])->SetBossStatus(BOSS_STATUS::PATTERN_SECOND);
					}
				}
			}
		}
	}

	// collision
	Collision();

	return 0;
}

void CScnMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glClearColor(0.f, 0.f, 0.f, 1.0f);

	if (m_pLogo != nullptr)
	{
		if (m_bGameStart && !m_bGameEnd)
		{
			m_bLogo = true;
			m_bGameStart = false;
			m_pLogo->SetTextureID(m_iTexture[12]);
		}
		else if (!m_bGameStart && m_bGameEnd)
		{
			m_bLogo = true;
			m_bGameEnd = false;
			m_pLogo->SetTextureID(m_iTexture[11]);
		}

		if (m_bLogo)
		{
			if (KEYTAB(KEY_TYPE::KEY_ENTER))
			{
				m_bLogo = false;
			}

			int iTextureID = m_pLogo->GetTextureID();
			m_pRenderer->DrawGround(0.f, 0.f, 0.f, 500.f, 500.f, 0.f, 1.f, 1.f, 1.f, 1.f, iTextureID, 1.f);
			return;
		}
	}

	if (!m_bParticle)
	{
		m_iParticleTexture[0] = m_pRenderer->GenPngTexture("../Resource/Texture/particleblue.png");
		m_iParticleTexture[1] = m_pRenderer->GenPngTexture("../Resource/Texture/particleblack.png");
		m_iParticleTexture[2] = m_pRenderer->GenPngTexture("../Resource/Texture/particlepurple.png");
		m_iParticleTexture[3] = m_pRenderer->GenPngTexture("../Resource/Texture/particleyellow.png");
		m_iParticleTexture[4] = m_pRenderer->GenPngTexture("../Resource/Texture/particlered.png");
		m_iParticleTexture[5] = m_pRenderer->GenPngTexture("../Resource/Texture/particleplayer.png");

		for (int i = 0; i < MAX_PARTICLE; ++i)
		{
			if (i == 5 || i == 4)
			{
				m_iParticleObj[i] = m_pRenderer->CreateParticleObject(1000, -0.5, -0.5, 0.5, 0.5, 3, 3, 5, 5, -75, -75, 75, 75);//속도도 픽셀기준 초당 몇 픽셀을 가냐픽셀 기준 1. 개수 2. 3. 4. 5. 6. 7. 8. 9.
				continue;
			}

			m_iParticleObj[i] = m_pRenderer->CreateParticleObject(200, -0.5, -0.5, 0.5, 0.5, 3, 3, 5, 5, -75, -75, 75, 75);//속도도 픽셀기준 초당 몇 픽셀을 가냐픽셀 기준 1. 개수 2. 3. 4. 5. 6. 7. 8. 9.
		}
		m_bParticle = true;
	}

	for (int i = 0; i < MAX_PARTICLE; ++i)
	{
		if (m_bParticleOn[i])
		{
			m_fParticleDuration[i] += DT;

			int inum = 200;
			if (i == 5)
				inum = 1000;

			m_pRenderer->DrawParticle(m_iParticleObj[i],
				100.f * m_vParticlePos[i].x, 100.f * m_vParticlePos[i].y, 0.f,
				0.5f,
				1, 1, 1, 1,
				0, 0,
				m_iParticleTexture[i],
				inum,//1000개면 1000개를 다쓰겟다는 의미 ratio
				m_fParticleDuration[i]);//피벗이 센터 마지막 인자는 파티클이 지속되는 시간


			if (i != 5 && m_fParticleDuration[i] > 1.2f) // 이전 속도와 현재 속도 비교해서 같으면 ~~로 해야한다.
			{
				m_bParticleOn[i] = false;
				m_fParticleDuration[i] = 0.f;
			}
			else if (i == 5 && m_fParticleDuration[i] > 3.f)
			{
				m_bParticleOn[i] = false;
				m_fParticleDuration[i] = 0.f;
			}
		}
	}
	

	if (nullptr != m_pUI)
	{
		if (-1 == m_pUI->GetTextureID())
		{
			m_pUI->SetTextureID(m_iTexture[10]);
		}

		Vec3 vUIPos = m_pUI->GetObjPos();
		Vec3 vUISize = m_pUI->GetObjSize();
		Vec4 vUIColor = m_pUI->GetObjColor();

		int iTextureID = m_pUI->GetTextureID();

		vUISize.x = 500.f*vUISize.x;
		vUISize.y = 500.f*vUISize.y;
		vUISize.z = 500.f*vUISize.z;

		m_pRenderer->DrawGround(0.f, 0.f, 0.f, vUISize.x, vUISize.y, 1.f, 1.f, 1.f, 1.f, 1.f, iTextureID, 2.f);
	}

	if (nullptr != m_pMap)
	{
		if (-1 == m_pMap->GetTextureID())
		{
			m_pMap->SetTextureID(m_iTexture[5]);

			m_eCurMap = MAP::START;

			AddObject(Vec3(2.1f, -0.35f, 0.f), Vec3(0.25f, 0.25f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 1.f, 0.f, OBJ_TYPE::DOOR);
			AddObject(Vec3(0.f,-2.1f, 0.f), Vec3(0.25f, 0.25f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 1.f, 0.f, OBJ_TYPE::DOOR);
			AddObject(Vec3(-2.1f, -0.35f, 0.f), Vec3(0.25f, 0.25f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 1.f, 0.f, OBJ_TYPE::DOOR);
			AddObject(Vec3(0.f, 1.2f, 0.f), Vec3(0.25f, 0.25f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 1.f, 0.f, OBJ_TYPE::DOOR);
		}

		Vec3 vMapPos = m_pMap->GetObjPos();
		Vec3 vMapSize = m_pMap->GetObjSize();
		Vec4 vMapColor = m_pMap->GetObjColor();

		int iTextureID = m_pMap->GetTextureID();

		vMapSize.x = 500.f*vMapSize.x;
		vMapSize.y = 450.f*vMapSize.y;
		vMapSize.z = 500.f*vMapSize.z;

		m_pRenderer->DrawGround(0.f, -25.f, 0.f, vMapSize.x, vMapSize.y, 0.f, 1.f, 1.f, 1.f, 1.f, iTextureID, 1.f);
	}


	for (int i = 0; i < MAX_DOOR; ++i)
	{
		if (nullptr != m_pDoor[i])
		{
			if (-1 == m_pDoor[i]->GetTextureID())
			{
				m_pDoor[i]->SetTextureID(m_iTexture[8]);

				//AddObject(Vec3(2.f, 0.f, 0.f), Vec3(0.25f, 0.25f, 0.5f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 0.f, 0.f, OBJ_TYPE::DOOR);
			}
		}

		//if (m_pDoor[i] == NULL)
		//	return;
		if (m_eCurMap == MAP::START && m_pDoor[i] != NULL)
		{
			((CDoor*)m_pDoor[0])->SetAnim(Vec2(1, 3));

			if (i == 0)
				((CDoor*)m_pDoor[i])->SetDoorOpen(true);
			else 
				((CDoor*)m_pDoor[i])->SetDoorOpen(false);
		}
		else if (m_eCurMap == MAP::ROOM1)
		{
			int iCount = 0;

			for (int i = 0; i < MAX_MONSTERS; ++i)
			{
				if (m_pMonster[i] == nullptr)
				{
					++iCount;
				}
			}

			if (MAX_MONSTERS == iCount)
			{
				((CDoor*)m_pDoor[0])->SetAnim(Vec2(1, 3));
			}
			else
			{
			((CDoor*)m_pDoor[0])->SetAnim(Vec2(0, 3));
			}


			if (i == 0 || i == 1 || i==2|| i ==3)
				((CDoor*)m_pDoor[i])->SetDoorOpen(true);
			else
				((CDoor*)m_pDoor[i])->SetDoorOpen(false);
		}
		else if (m_eCurMap == MAP::BOSSROOM)
		{
			if (i == 2)
				((CDoor*)m_pDoor[i])->SetDoorOpen(true);
			else
				((CDoor*)m_pDoor[i])->SetDoorOpen(false);
		}

		if (m_pDoor[i] == NULL)
			return;

		int iTexId = m_pDoor[i]->GetTextureID();
		if (iTexId < 0)
			return;

		Vec3 vPos = 100.f*m_pDoor[i]->GetObjPos();

		if (((CDoor*)m_pDoor[i])->GetDoorOpen())
		{
			Vec2 vAnim = ((CDoor*)m_pDoor[i])->GetAnim();
			if(m_eCurMap == MAP::BOSSROOM)
				m_pRenderer->DrawTextureRectAnim(vPos.x, vPos.y, vPos.z, 40.f, 40.f, 0.f, 1.f, 1.f, 1.f, 1.f, iTexId, 2, 4, 0, 2, false);
			else
				m_pRenderer->DrawTextureRectAnim(vPos.x, vPos.y, vPos.z, 40.f, 40.f, 0.f, 1.f, 1.f, 1.f, 1.f, iTexId, 2, 4, vAnim.x, vAnim.y, false);
		}
	}

	if (nullptr != m_pPlayer)
	{
		if (-1 == m_pPlayer->GetTextureID())
		{
			m_pPlayer->SetTextureID(m_iTexture[0]);
		}

		Vec3 vPlayerPos = m_pPlayer->GetObjPos();
		Vec3 vPlayerSize = m_pPlayer->GetObjSize();
		Vec4 vPlayerColor = m_pPlayer->GetObjColor();

		int iTextureID = m_pPlayer->GetTextureID();

		vPlayerPos.x = 100.f*vPlayerPos.x;
		vPlayerPos.y = 100.f*vPlayerPos.y;
		vPlayerPos.z = 100.f*vPlayerPos.z;

		vPlayerSize.x = 100.f*vPlayerSize.x;
		vPlayerSize.y = 100.f*vPlayerSize.y;
		vPlayerSize.z = 100.f*vPlayerSize.z;

		int iTexId = m_pPlayer->GetTextureID();

		if (iTexId < 0)
			return;

		Vec2 iPlayerBody = ((CPlayer*)m_pPlayer)->GetBodyIdx();
		Vec2 iPlayerHead = ((CPlayer*)m_pPlayer)->GetHeadIdx();

		m_pRenderer->DrawTextureRectAnim(vPlayerPos.x, vPlayerPos.y, vPlayerPos.z, vPlayerSize.x, vPlayerSize.y, vPlayerSize.z,
			vPlayerColor.x, vPlayerColor.y, vPlayerColor.z, vPlayerColor.w, iTexId, 8, 6, iPlayerBody.x, iPlayerBody.y);

		m_pRenderer->DrawTextureRectAnim(vPlayerPos.x, vPlayerPos.y + 8.f, vPlayerPos.z, vPlayerSize.x, vPlayerSize.y, vPlayerSize.z,
			vPlayerColor.x, vPlayerColor.y, vPlayerColor.z, vPlayerColor.w, iTexId, 8, 6, iPlayerHead.x, iPlayerHead.y, false);


		for (int i = 0; i < MAX_LIFE; ++i)
		{
			Vec3 vLifePos = m_pLife[i]->GetObjPos();
			Vec3 vLifeSize = m_pLife[i]->GetObjSize();
			Vec4 vLifeColor = m_pLife[i]->GetObjColor();

			if (((CLife*)m_pLife[i])->GetAlive())
			{	
				m_pRenderer->DrawTextureRectAnim(vLifePos.x, vLifePos.y-8.f, vLifePos.z, vLifeSize.x*2.5f, vLifeSize.y*2.5f, 0.f, 1.f, 1.f, 1.f, 1.f, m_iTexture[13], 2, 2, 0, 0, false);
			}
			else
				int k = 1;
		}
		//m_pRenderer->SetCameraPos(vPlayerPos.x, vPlayerPos.y); // 카메라 플레이어 고정
	}

	for (int i = 0; i < MAX_BULLETS; ++i) // 끌때 삭제 문제생김
	{
		// RenderBullet();
		if (nullptr != m_pBullet[i])
		{
			if (-1 == m_pBullet[i]->GetTextureID()) // 불필요한 조건문인듯 한데 반복되네
			{
				m_pBullet[i]->SetTextureID(m_iTexture[1]);
			}

			Vec3 vBulletPos = m_pBullet[i]->GetObjPos();
			Vec3 vBulletSize = m_pBullet[i]->GetObjSize();
			Vec4 vBulletColor = m_pBullet[i]->GetObjColor();

			vBulletPos.x = 100.f*vBulletPos.x;
			vBulletPos.y = 100.f*vBulletPos.y;
			vBulletPos.z = 100.f*vBulletPos.z;

			vBulletSize.x = 100.f*vBulletSize.x;
			vBulletSize.y = 100.f*vBulletSize.y;
			vBulletSize.z = 100.f*vBulletSize.z;

			int iTexId = m_pBullet[i]->GetTextureID();

			if (iTexId < 0)
				return;

			Vec2 iBulletIdx = m_pBullet[i]->GetAnimIdx();

			m_pRenderer->DrawTextureRectAnim(vBulletPos.x, vBulletPos.y, vBulletPos.z, vBulletSize.x, vBulletSize.y, vBulletSize.z,
				vBulletColor.x, vBulletColor.y, vBulletColor.z, vBulletColor.w, iTexId, 16, 1, iBulletIdx.x, iBulletIdx.y); // 
		}
	}

	for (int i = 0; i < MAX_MONSTERS; ++i)
	{
		// RenderBullet();
		if (nullptr != m_pMonster[i])
		{
			if (-1 == m_pMonster[i]->GetTextureID()) // 불필요한 조건문인듯 한데 반복되네
			{
				if (m_eCurMap == MAP::BOSSROOM)
				{
					if (0 == i)
					{
						m_pMonster[i]->SetTextureID(m_iTexture[2]);
					}
					else
					{
						m_pMonster[i]->SetTextureID(m_iTexture[3]);
					}
				}
				else if (m_eCurMap == MAP::ROOM1)
				{
					m_pMonster[i]->SetTextureID(m_iTexture[4]);
				}
			}

			Vec3 vMonsterPos = m_pMonster[i]->GetObjPos();
			Vec3 vMonsterSize = m_pMonster[i]->GetObjSize();
			Vec4 vMonsterColor = m_pMonster[i]->GetObjColor();

			vMonsterPos.x = 100.f*vMonsterPos.x;
			vMonsterPos.y = 100.f*vMonsterPos.y;
			vMonsterPos.z = 100.f*vMonsterPos.z;

			vMonsterSize.x = 100.f*vMonsterSize.x;
			vMonsterSize.y = 100.f*vMonsterSize.y;
			vMonsterSize.z = 100.f*vMonsterSize.z;

			int iTexId = m_pMonster[i]->GetTextureID();

			Vec2 iMonsterAnimIdx = m_pMonster[i]->GetAnimIdx();

			if (iTexId < 0)
				return;
			
			int iHp = (int)m_pMonster[i]->GetHp();
			float fRatio = 0.f;

			if (m_eCurMap == MAP::BOSSROOM)
			{
				if (m_pMonster[i]->GetObjType() == OBJ_TYPE::BOSS)
				{
					m_pRenderer->DrawTextureRectAnim(vMonsterPos.x, vMonsterPos.y, vMonsterPos.z, vMonsterSize.x, vMonsterSize.y, vMonsterSize.z,
						vMonsterColor.x, vMonsterColor.y, vMonsterColor.z, vMonsterColor.w, iTexId, 4, 3, iMonsterAnimIdx.x, iMonsterAnimIdx.y);

					static float ibossratio = iHp / 100;
					fRatio = (int)(iHp / ibossratio);
				}
				else
				{
					m_pRenderer->DrawTextureRectAnim(vMonsterPos.x, vMonsterPos.y, vMonsterPos.z, vMonsterSize.x, vMonsterSize.y, vMonsterSize.z,
						vMonsterColor.x, vMonsterColor.y, vMonsterColor.z, vMonsterColor.w, iTexId, 4, 4, iMonsterAnimIdx.x, iMonsterAnimIdx.y);

					static float imonsterratio = iHp / 10;
					fRatio = (int)(iHp / imonsterratio) * 10;
				}
			}
			else if (m_eCurMap == MAP::ROOM1)
			{
				m_pRenderer->DrawTextureRectAnim(vMonsterPos.x, vMonsterPos.y, vMonsterPos.z, vMonsterSize.x, vMonsterSize.y, vMonsterSize.z,
					vMonsterColor.x, vMonsterColor.y, vMonsterColor.z, vMonsterColor.w, iTexId, 3, 4, iMonsterAnimIdx.x, iMonsterAnimIdx.y);

				static float imonsterratio = iHp / 10;
				fRatio = (int)(iHp / imonsterratio) * 10;
			}

			m_pRenderer->DrawSolidRectGauge(vMonsterPos.x, vMonsterPos.y + 10.f, vMonsterPos.z, 0.f, 15.f, 0.f, vMonsterSize.x, vMonsterSize.y*0.125f, 0.f, 1.f, 0.f, 0.f, 1.f, fRatio);
			m_pRenderer->DrawSolidRectBorder(vMonsterPos.x, vMonsterPos.y + 40.f, vMonsterPos.z, vMonsterSize.x, vMonsterSize.y*0.125f, 0.f, 1.f, 0.f, 0.f, 1.f);

		}
	}
}

int CScnMgr::AddObject(Vec3 vPos, Vec3 vSize, Vec4 vColor, Vec3 vVel, float mass, float fricCoef, OBJ_TYPE eObjType, CObject* _pObject)
{
	// Search empty slot

	// AddPlayerObj
	if (OBJ_TYPE::PLAYER == eObjType)
	{
		if (nullptr != m_pPlayer)
			return false;
		else if (nullptr == m_pPlayer)
		{
			m_pPlayer = new CPlayer();
			m_pPlayer->SetObjPos(vPos);
			m_pPlayer->SetObjSize(vSize);
			m_pPlayer->SetObjColor(vColor);
			m_pPlayer->SetObjType(eObjType);
			m_pPlayer->SetObjMass(mass);
			m_pPlayer->SetObjFricCoef(fricCoef);
			//m_pPlayer->SetObjHp(100.f);
			m_pPlayer->SetBulletCoolTime(2.f);

			for (int i = 0; i < MAX_LIFE; ++i)
			{
				m_pLife[i] = new CLife();
				m_pLife[i]->SetObjPos(Vec3(130.f + i * 20.f, 215.f, 0.f));
				m_pLife[i]->SetObjSize(Vec3(10.f, 10.f, 0.f));
				m_pLife[i]->SetObjColor(Vec4(1.f, 0.f, 0.f, 1.f));
				m_pLife[i]->SetObjType(OBJ_TYPE::LIFE);
				m_pLife[i]->SetObjMass(1.f);
				m_pLife[i]->SetTextureID(m_iTexture[13]);
				((CLife*)m_pLife[i])->SetAlive(true);
				((CLife*)m_pLife[i])->SetLifeCount(0);
			}

			AddObject(Vec3(0.f, 0.f, 0.f), Vec3(1.f, 1.f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 0.f, 0.f, OBJ_TYPE::UI);

			return true;
		}
	}

	if (OBJ_TYPE::MAP == eObjType)
	{
		if (nullptr != m_pMap)
			return false;
		else if (nullptr == m_pMap)
		{
			m_pMap = new CMap();
			m_pMap->SetObjPos(vPos);
			m_pMap->SetObjSize(vSize);
			m_pMap->SetObjColor(vColor);
			m_pMap->SetObjType(eObjType);

			return true;
		}
	}

	if (OBJ_TYPE::UI == eObjType)
	{
		if (nullptr != m_pUI)
			return false;
		else if (nullptr == m_pUI)
		{
			m_pUI = new CMap();
			m_pUI->SetObjPos(vPos);
			m_pUI->SetObjSize(vSize);
			m_pUI->SetObjColor(vColor);
			m_pUI->SetObjType(eObjType);

			return true;
		}
	}

	if (OBJ_TYPE::LOGO == eObjType)
	{
		if (nullptr != m_pLogo)
			return false;
		else if (nullptr == m_pLogo)
		{
			m_pLogo = new CMap();
			m_pLogo->SetObjPos(vPos);
			m_pLogo->SetObjSize(vSize);
			m_pLogo->SetObjColor(vColor);
			m_pLogo->SetObjType(eObjType);

			return true;
		}
	}

	if (OBJ_TYPE::DOOR == eObjType)
	{
		int iDoorIdx = -1;

		for (int i = 0; i < MAX_DOOR; ++i)
		{
			if (m_pDoor[i] == NULL)
			{
				iDoorIdx = i;
				break;
			}
		}

		m_pDoor[iDoorIdx] = new CDoor();
		m_pDoor[iDoorIdx]->SetObjPos(vPos);
		m_pDoor[iDoorIdx]->SetObjSize(vSize);
		m_pDoor[iDoorIdx]->SetObjColor(vColor);
		m_pDoor[iDoorIdx]->SetObjType(eObjType);

		if (0 == iDoorIdx)
			((CDoor*)m_pDoor[iDoorIdx])->SetAnim(Vec2(1, 3));
		else if (1 == iDoorIdx)
			((CDoor*)m_pDoor[iDoorIdx])->SetAnim(Vec2(0, 1));
		else if (2 == iDoorIdx)
			((CDoor*)m_pDoor[iDoorIdx])->SetAnim(Vec2(1, 2));
		else if (3 == iDoorIdx)
			((CDoor*)m_pDoor[iDoorIdx])->SetAnim(Vec2(0, 0));

		return true;
	}

	// AddBulletObj
	if (OBJ_TYPE::BULLET == eObjType)
	{
		int iBulletidx = -1;

		for (int i = 0; i < MAX_BULLETS; ++i)
		{
			if (m_pBullet[i] == NULL)
			{
				iBulletidx = i;
				break;
			}
		}

		if (iBulletidx == -1)
		{
			std::cout << "No more remaining bullet \n";
			return -1;
		}

		m_pBullet[iBulletidx] = new CBullet();
		m_pBullet[iBulletidx]->SetObjPos(vPos);
		m_pBullet[iBulletidx]->SetObjSize(vSize);
		m_pBullet[iBulletidx]->SetObjColor(vColor);
		m_pBullet[iBulletidx]->SetObjType(eObjType);
		m_pBullet[iBulletidx]->SetObjMass(mass);
		m_pBullet[iBulletidx]->SetObjFricCoef(fricCoef);
		m_pBullet[iBulletidx]->SetForce(_pObject->GetForce());
		m_pBullet[iBulletidx]->SetObjVel(_pObject->GetObjVel());

		((CBullet*)m_pBullet[iBulletidx])->SetPlayer(m_pPlayer);

		if (OBJ_TYPE::PLAYER == _pObject->GetObjType())
		{
			((CBullet*)m_pBullet[iBulletidx])->SetOwner(_pObject);
		}
		else if (OBJ_TYPE::MONSTER == _pObject->GetObjType() )
		{
			CWell512 random;

			//((CBullet*)m_pBullet[iBulletidx])->SetBulletType((BULLET_TYPE)random.GetValue(1,4)); // 
			((CBullet*)m_pBullet[iBulletidx])->SetBulletType(BULLET_TYPE::GUIDED);
			((CBullet*)m_pBullet[iBulletidx])->SetOwner(_pObject);
		}
		else if (OBJ_TYPE::BOSS == _pObject->GetObjType())
		{		
			((CBullet*)m_pBullet[iBulletidx])->SetBulletType(BULLET_TYPE::MULTI);
			((CBullet*)m_pBullet[iBulletidx])->SetOwner(_pObject);
		}

		return iBulletidx;
	}


	// AddMonster
	if (OBJ_TYPE::MONSTER == eObjType || OBJ_TYPE::BOSS == eObjType)
	{
		int iMonsterIdx = -1;

		for (int i = 0; i < MAX_MONSTERS; ++i)
		{
			if (m_pMonster[i] == NULL)
			{
				iMonsterIdx = i;
				break;
			}
		}

		if (iMonsterIdx == -1)
		{
			std::cout << "No more remaining monster \n";
			return -1;
		}

		m_pMonster[iMonsterIdx] = new CMonster();
		m_pMonster[iMonsterIdx]->SetObjPos(vPos);
		m_pMonster[iMonsterIdx]->SetObjSize(vSize);
		m_pMonster[iMonsterIdx]->SetObjColor(vColor);
		m_pMonster[iMonsterIdx]->SetObjVel(vVel);
		m_pMonster[iMonsterIdx]->SetObjType(eObjType);
		m_pMonster[iMonsterIdx]->SetObjMass(mass);
		m_pMonster[iMonsterIdx]->SetObjFricCoef(fricCoef);
		m_pMonster[iMonsterIdx]->SetBulletCoolTime(2.f);

		if (OBJ_TYPE::BOSS == eObjType)
		{
			m_pMonster[iMonsterIdx]->SetObjHp(BOSS_HP);
			((CMonster*)m_pMonster[iMonsterIdx])->SetMonsterType(MONSTER_TYPE::BOSS);
		}
		else
		{
			m_pMonster[iMonsterIdx]->SetOwner(_pObject);

			if (nullptr != _pObject)
				((CMonster*)m_pMonster[iMonsterIdx])->SetMonsterType(MONSTER_TYPE::GHOST);
			else
				((CMonster*)m_pMonster[iMonsterIdx])->SetMonsterType(MONSTER_TYPE::DEVIL);
		
			m_pMonster[iMonsterIdx]->SetObjHp(30.f);
		}
		m_pMonster[iMonsterIdx]->SetBulletCoolTime(2.f);

		return iMonsterIdx;
	}

	return -1;
}

void CScnMgr::DeleteObject(int idx, CObject* pObject)
{
	if (pObject == NULL)
		return;

	if (OBJ_TYPE::BULLET == pObject->GetObjType())
	{
		if (m_pBullet[idx] != NULL)
		{
			SAFE_DELETE(m_pBullet[idx]);
		}
	}

	if (OBJ_TYPE::MONSTER == pObject->GetObjType() || OBJ_TYPE::BOSS == pObject->GetObjType())
	{
		if (m_pMonster[idx] != NULL)
		{
			SAFE_DELETE(m_pMonster[idx]);

			if (idx == 0)
			{
				Sound::GetInst()->PlayShortSound(m_iSound[4], false, 1.f);

			}
		}
	}

	if (OBJ_TYPE::PLAYER == pObject->GetObjType())
	{
		m_pPlayer->SetTextureID(-1);
		SAFE_DELETE(m_pPlayer);
	}


	if (OBJ_TYPE::MAP == pObject->GetObjType())
	{
		if (m_pMap != NULL)
		{
			SAFE_DELETE(m_pMap);
		}
	}

	if (OBJ_TYPE::DOOR == pObject->GetObjType())
	{
		if (m_pDoor[idx] != NULL)
		{
			SAFE_DELETE(m_pDoor[idx]);
		}
	}

	if (idx < 0)
	{
		std::cout << "Negative idx does not allowed. \n";
		return;
	}

	if (idx >= MAX_OBJECTS)
	{
		std::cout << "Requested idx exceeds MAX_OBJECTS count. idx: " << idx << " \n";
		return;
	}
}

void CScnMgr::DoGarbageCollection()
{
	for (int i = 0; i < MAX_BULLETS; ++i)
	{
		if (m_pBullet[i] != NULL)
		{
			int type;
			type = (int)m_pBullet[i]->GetObjType(); // 삭제 과정에서 문제
			if (type == (int)OBJ_TYPE::BULLET)
			{
				Vec3 vVel = Vec3(0.f, 0.f, 0.f);
				vVel = m_pBullet[i]->GetObjVel();
				vVel.z = 0.f;
				float fSize = sqrt(vVel.x*vVel.x + vVel.y *vVel.y + vVel.z*vVel.z);

				if ((fSize < FLT_EPSILON && !((CBullet*)m_pBullet[i])->GetCollided()) || m_pBullet[i]->GetAnimEnd())
				{
					DeleteObject(i, m_pBullet[i]);
				}
			}
		}
	}

	for (int i = 0; i < MAX_MONSTERS; ++i)
	{
		if ((m_pMonster[i] != NULL && m_pMonster[i]->GetHp() < FLT_EPSILON))
		{
			if(((CMonster*)m_pMonster[i])->GetMonsterType() == MONSTER_TYPE::GHOST)
				Sound::GetInst()->PlayShortSound(m_iSound[1], false, 1.f);
			else if (((CMonster*)m_pMonster[i])->GetMonsterType() == MONSTER_TYPE::DEVIL)
				Sound::GetInst()->PlayShortSound(m_iSound[3], false, 1.f);

			DeleteObject(i, m_pMonster[i]);
		}
	}

	if ((m_pPlayer != nullptr && !((CLife*)m_pLife[0])->GetAlive())) //m_pPlayer->GetHp() < FLT_EPSILON)) // 라이브 0일때 죽게
	{
		m_bParticleOn[5] = true;
		m_vParticlePos[5] = m_pPlayer->GetObjPos();
		m_bCameraShake = false;
		DeleteObject(-100, m_pPlayer);
	}

	if (m_pPlayer == nullptr)
	{
		m_bResetOn = true;
	}
}

void CScnMgr::Collision()
{
	// 몬스터 플레이어 충돌 1초간 무적 상태로 바꿔야겟음
	for (int source = 0; source < MAX_MONSTERS; ++source)
	{
		if (m_pPlayer != nullptr && m_pMonster[source] != nullptr)
		{
			bool bCollided = CPhysics::GetInst()->IsCollision(m_pPlayer, m_pMonster[source], 0);
			if (bCollided && m_pPlayer->GetObjStatus() != OBJ_STATUS::INVINCIBILITY)
			{
				m_pPlayer->SetObjStatus(OBJ_STATUS::INVINCIBILITY);

				CPhysics::GetInst()->ProcessCollision(m_pPlayer, m_pMonster[source]);

				((CPlayer*)m_pPlayer)->SetMinusLife();

				if (!((CLife*)m_pLife[0])->GetAlive()) // 죽음 처리
					return;

				((CLife*)m_pLife[((CLife*)m_pLife[0])->GetLifeCount() - 1])->SetAlive(false);
				for (int i = 0; i < MAX_LIFE; ++i)
				{
					((CLife*)m_pLife[i])->SetLifeCount(1);
				}

				m_bParticleOn[4] = true;
				m_vParticlePos[4] = (m_pPlayer->GetObjPos() + m_pMonster[source]->GetObjPos())*0.5f;

				Sound::GetInst()->PlayShortSound(m_iSound[2], false, 2.f);
			}
		}
	}

	// 몬스터 vs 총알 충돌 // 지네끼리 붙는건 빼도될듯
	for (int source = 0; source < MAX_MONSTERS; ++source)
	{
		for (int target = 0; target < MAX_BULLETS; ++target)
		{
			if (m_pBullet[target] != nullptr && m_pMonster[source] != nullptr)
			{
				if ((m_pBullet[target])->GetOwner() == m_pMonster[source] /*&& m_pBullet[target]->GetOwner()->GetObjType() == OBJ_TYPE::MONSTER*/) // m_pBullet[target])->IsAncestor(m_pMonster[source]) && m_pMonster[source]->IsAncestor(m_pBullet[target]
					continue;
				
				bool bCollide = CPhysics::GetInst()->IsCollision(m_pBullet[target], m_pMonster[source], 0);
				if (bCollide && !((CBullet*)m_pBullet[target])->GetCollided() && m_pMonster[source]->GetObjStatus() != OBJ_STATUS::INVINCIBILITY)
				{
					m_pMonster[source]->SetObjStatus(OBJ_STATUS::INVINCIBILITY);

					CPhysics::GetInst()->ProcessCollision(m_pBullet[target], m_pMonster[source]);
					if (m_pPlayer != nullptr && m_pBullet[target]->GetOwner() == m_pPlayer)
					{
						((CBullet*)m_pBullet[target])->SetCollided(true);
						m_pMonster[source]->SetHp(m_pBullet[target]->GetOffensePower());

						CWell512 random;
						int iIdx = 0;
						int iCount = 0;

						while (m_bParticleOn[iIdx])
						{
							if (iCount > 10)
								break;

							iIdx = random.GetValue(0, 4);
							++iCount;
						}

						m_bParticleOn[iIdx] = true;
						m_vParticlePos[iIdx] = m_pBullet[target]->GetObjPos();				
					}

					Sound::GetInst()->PlayShortSound(m_iSound[0], false, 1.f);
				}
			}
		}
	}

	// 플레이어 vs 몬스터 총알
	for (int i = 0; i < MAX_BULLETS; ++i)
	{
		if (m_pBullet[i] != nullptr && m_pPlayer != nullptr)
		{
			if (m_pBullet[i]->GetOwner() == m_pPlayer)
				continue;

			bool bCollide = CPhysics::GetInst()->IsCollision(m_pBullet[i], m_pPlayer, 0);
			if (bCollide && !((CBullet*)m_pBullet[i])->GetCollided() && m_pPlayer->GetObjStatus() != OBJ_STATUS::INVINCIBILITY)
			{
				m_pPlayer->SetObjStatus(OBJ_STATUS::INVINCIBILITY);

				CPhysics::GetInst()->ProcessCollision(m_pBullet[i], m_pPlayer);
				m_pPlayer->SetHp(m_pBullet[i]->GetOffensePower());
				((CBullet*)m_pBullet[i])->SetCollided(true);
				((CPlayer*)m_pPlayer)->SetMinusLife();

				if (!((CLife*)m_pLife[0])->GetAlive()) // 죽음 처리
					return;

				((CLife*)m_pLife[((CLife*)m_pLife[0])->GetLifeCount() - 1])->SetAlive(false);
				for (int i = 0; i < MAX_LIFE; ++i)
				{
					((CLife*)m_pLife[i])->SetLifeCount(1);
				}

				Sound::GetInst()->PlayShortSound(m_iSound[2], false, 2.f);
			}
		}
	}

	for (int i = 0; i < MAX_DOOR; ++i)
	{
		if (m_pPlayer != nullptr && m_pDoor[i] != nullptr)
		{
			bool bCollide = CPhysics::GetInst()->IsCollision(m_pDoor[i], m_pPlayer, 0);
			if (bCollide)
			{
				if (m_eCurMap == MAP::START)
				{
					if (0 == i)
					{
						m_eCurMap = MAP::ROOM1;
						m_eBeforeMap = MAP::START;
						m_pMap->SetTextureID(m_iTexture[6]);

						m_pPlayer->SetObjPos(Vec3(-1.8f, -0.25f, 0.f));

						CWell512 well512;
						for (int i = 0; i < ROOM_MONSTER; ++i)  // 몬스터 생성
						{
							float vPosX = well512.GetFloatValue(-2, 2);
							float vPosY = well512.GetFloatValue(-2, 2);
							CScnMgr::GetInst()->AddObject(Vec3(vPosX, vPosY, 0.f), Vec3(0.25f, 0.25f, 0.3f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 10.f, 1.3f, OBJ_TYPE::MONSTER, nullptr);
						}

						Sound::GetInst()->PlayShortSound(m_iSound[6], false, 2.f);
						return;
					}
				}
				else if (m_eCurMap == MAP::ROOM1)
				{
					if (i == 0)
					{
						Vec2 vIdx = ((CDoor*)m_pDoor[i])->GetAnim();
						if (vIdx.x == 1)
						{
							Sound::GetInst()->PlayShortSound(m_iSound[6], false, 2.f);

							m_eCurMap = MAP::BOSSROOM;
							m_eBeforeMap = MAP::ROOM1;
							m_pMap->SetTextureID(m_iTexture[7]);

							m_pPlayer->SetObjPos(Vec3(-1.8f, -0.25f, 0.f));

							for (int i = 0; i < MAX_MONSTERS; ++i)
							{
								DeleteObject(i, m_pMonster[i]);
							}

							for (int i = 0; i < MAX_MONSTERS; ++i)
							{
								if (i == 0)
								{
									Vec3 vPos = Vec3(1.f, 0.f, 0.f);
									float vSize = 0.5f;

									AddObject(Vec3(vPos.x, vPos.y - 0.25f, 0.f), Vec3(vSize, vSize, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 10.f, 0.3f, OBJ_TYPE::BOSS);
								}
							}

							Sound::GetInst()->DeleteBGSound(g_isound);
							g_isound = Sound::GetInst()->CreateBGSound("../Resource/Sound/BossBgm2.mp3");
							Sound::GetInst()->PlayBGSound(g_isound, true, 1.0f);//2번째 인자는 루프 3번쨰인자는 볼륨
						}
						return;
					}
					else if (i == 2)
					{
						Sound::GetInst()->PlayShortSound(m_iSound[6], false, 2.f);

						m_eCurMap = MAP::START;
						m_eBeforeMap = MAP::ROOM1;
						m_pMap->SetTextureID(m_iTexture[5]);

						for (int i = 0; i < MAX_MONSTERS; ++i)
						{
							DeleteObject(i,m_pMonster[i]);
						}

						m_pPlayer->SetObjPos(Vec3(1.8f, -0.25f, 0.f));

						return;
					}
				}
			}
		}
	}
}

void CScnMgr::DeleteScene()
{
	for (int i = 0; i < MAX_BULLETS; ++i)
	{
		if (nullptr != m_pBullet[i])
			SAFE_DELETE(m_pBullet[i]);
	}

	for (int i = 0; i < MAX_MONSTERS; ++i)
	{
		if (nullptr != m_pMonster[i])
			SAFE_DELETE(m_pMonster[i]);
	}

	for (int i = 0; i < MAX_DOOR; ++i)
	{
		if (nullptr != m_pDoor[i])
			SAFE_DELETE(m_pDoor[i]);
	}

	for (int i = 0; i < MAX_LIFE; ++i)
	{
		if (nullptr != m_pLife[i])
			SAFE_DELETE(m_pLife[i]);
	}

	if (nullptr != m_pPlayer)
		SAFE_DELETE(m_pPlayer);

	if (nullptr != m_pMap)
		SAFE_DELETE(m_pMap);

	if (nullptr != m_pUI)
		SAFE_DELETE(m_pUI);

	if (nullptr != m_pLogo)
		SAFE_DELETE(m_pLogo);

	if (nullptr != m_pSound)
		SAFE_DELETE(m_pSound);


	if (nullptr != m_pRenderer)
		SAFE_DELETE(m_pRenderer);
}

CScnMgr::CScnMgr(const CScnMgr & _other)
{
	*this = _other;
}

void CScnMgr::CemeraShaking()
{
	if (m_bCameraShake)
	{
		m_fShakingTimer += DT;
		int iTime = m_fShakingTimer * 100;

		//cout << iTime << endl;

		if (iTime % 2 == 0)
		{
			m_vCamPos.x += 2.f;
		}
		else if(iTime % 2 == 1)
		{
			m_vCamPos.x -= 2.f;
		}

		if (m_fShakingTimer >= 0.4f)
		{
			m_bCameraShake = false;
			m_fShakingTimer = 0.f;
			m_pRenderer->SetCameraPos(0.f, 0.f);
			m_vCamPos = Vec2(0.f, 0.f);
			return;
		}
		m_pRenderer->SetCameraPos(m_vCamPos.x, 0.f);	
	}
}

// 사용 x
void CScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	if ('w' == key || 'W' == key)
	{
		m_bKeyW = true;
	}
	else if ('s' == key || 'S' == key)
	{
		m_bKeyS = true;
	}
	else if ('a' == key || 'A' == key)
	{
		m_bKeyA = true;
	}
	else if ('d' == key || 'D' == key)
	{
		m_bKeyD = true;
	}
}

// 사용 x
void CScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	if ('w' == key || 'W' == key)
	{
		m_bKeyW = false;
	}
	else if ('s' == key || 'S' == key)
	{
		m_bKeyS = false;
	}
	else if ('a' == key || 'A' == key)
	{
		m_bKeyA = false;
	}
	else if ('d' == key || 'D' == key)
	{
		m_bKeyD = false;
	}
}

// 사용 x
void CScnMgr::SpecialKeyDownInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_bKeyUp = true;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_bKeyDown = true;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_bKeyLeft = true;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_bKeyRight = true;
	}
}

// 사용 x
void CScnMgr::SpecialKeyUpInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_bKeyUp = false;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_bKeyDown = false;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_bKeyLeft = false;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_bKeyRight = false;
	}
}