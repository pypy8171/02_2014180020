#pragma once
#include "Object.h"
class CDoor :
	public CObject
{
private:
	bool		m_bOpen;
	Vec2		m_vAnim;
public:
	virtual int update();
	virtual void init();

public:
	void SetDoorOpen(bool _b) { m_bOpen = _b; }
	const bool& GetDoorOpen() { return m_bOpen; }

	void SetAnim(Vec2 _anim) { m_vAnim = _anim; }
	const Vec2& GetAnim() { return m_vAnim; }

public:
	CDoor();
	virtual ~CDoor();
};

