#pragma once

#include <d3d11.h>


#include <list>
#include <vector>
#include <map>

using std::list;
using std::vector;
using std::map;

#include <array>
#include "SimpleMath.h"

#include <string>
#include <typeinfo>

using namespace std;
using namespace DirectX;

#include <cassert>
#include "struct.h"
#include "define.h"
#include "func.h"

#include "extern.h"
#include "well512.h"
