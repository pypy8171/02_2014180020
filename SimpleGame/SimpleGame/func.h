#include "global.h"

#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"

template<typename T>
void Safe_Delete_Vec(vector<T>& _vec)
{
	for (UINT i = 0; i < _vec.size(); ++i)
	{
		if (nullptr != _vec[i])
		{
			delete _vec[i];
			_vec[i] = nullptr;
		}
	}
	_vec.clear();
}


template<typename T>
void CallBack(T *callbackData)
{
	//T* pThis = (T*)callbackData;
	T* pThis = new T(*callbackData);
	delete pThis;

	glutLeaveMainLoop();
	glutDestroyWindow(0);
}

