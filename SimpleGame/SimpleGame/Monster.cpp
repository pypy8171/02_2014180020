#include "stdafx.h"
#include "Monster.h"

#include "TimeMgr.h"
#include "KeyMgr.h"
#include "ScnMgr.h"
#include "Sound.h"

CWell512 VelRandom;
CWell512 well512;

CMonster::CMonster()
	: m_eMonsterType(MONSTER_TYPE::NONE)
	, m_pPlayer(nullptr)
	, m_vDir(Vec3(0.f,0.f,0.f))
	, m_vPrevPos(Vec3(0.f, 0.f, 0.f))
	, m_vPlayerPos(Vec3(0.f, 0.f, 0.f))
	, m_bShootBullet(false)
	, m_fJumpCoolTime(0.f)
	, m_vDist(Vec3(0.f,0.f,0.f))
	, m_fDirTimer(0.f)
	, m_bLastPattern(false)
	, m_bAnimChange(false)
{
	init();
}

CMonster::~CMonster()
{

}

void CMonster::init()
{
	SetMonsterType(MONSTER_TYPE::NONE);
	SetRemainingBulletCoolTime(2.f);

	m_iDir = well512.GetValue(0, 4);

	if (GetObjType() == OBJ_TYPE::BOSS)
	{
		m_eBossStatus = BOSS_STATUS::PATTERN_FIRST;
	}
}

int CMonster::update()
{
	CObject::update();

	m_vPrevPos = GetObjPos();

	m_fAccTime += DT;

	Status();
	NormalMove();
	Animation();
	//Attack();

	m_vDir = (GetObjPos() - m_vPrevPos);
	m_vDir.Normalize();

	if (GetObjType() == OBJ_TYPE::BOSS)
	{
		BossPattern();
		ActionPattern(m_eBossStatus);
	}

	if (GetMonsterType() == MONSTER_TYPE::DEVIL)
	{
		MonsterAttack();
	}

	//if(GetOwner() != nullptr && GetOwner() != this && ((CMonster*)GetOwner())->GetBossStatus() == BOSS_STATUS::PATTERN_FOURTH)
	//	ChangeMonster();
	
	return 0;
}

void CMonster::BossPattern()
{
	int iHp = GetHp();

	static int iratio = iHp / 100;
	float fRatio = iHp / iratio;

	if (fRatio > 80)
	{
		m_eBossStatus = BOSS_STATUS::PATTERN_FIRST;
	}
	else if (fRatio > 60 && fRatio <= 80)
	{
		m_eBossStatus = BOSS_STATUS::PATTERN_SECOND;
	}
	else if (fRatio > 25 && fRatio <= 60)
	{
		m_eBossStatus = BOSS_STATUS::PATTERN_THIRD;
	}
	else if (fRatio <= 25)
	{
		m_eBossStatus = BOSS_STATUS::PATTERN_FOURTH;
	}
}

void CMonster::ActionPattern(BOSS_STATUS eStatus)
{
	switch (eStatus)
	{
	case BOSS_STATUS::PATTERN_FIRST:
		BossMove();
		break;
	case BOSS_STATUS::PATTERN_SECOND:
		BossMove();
		break;
	case BOSS_STATUS::PATTERN_THIRD:
		BossMove();
		break;
	case BOSS_STATUS::PATTERN_FOURTH:
		ChangeMonster();
		BossMove();
		break;
	default:
		break;
	}
}

void CMonster::Status()
{
	if (GetObjStatus() == OBJ_STATUS::INVINCIBILITY)
	{
		m_fInvinDT += DT;

		if (m_fInvinDT - m_fBeforeInvinDT > 0.05f)
			m_fBeforeInvinDT = m_fInvinDT;

		if (m_fBeforeInvinDT == m_fInvinDT)
			SetObjColor(Vec4(0.f, 0.f, 0.f, 0.f));
		else if (m_fBeforeInvinDT != m_fInvinDT)
			SetObjColor(Vec4(1.f, 1.f, 1.f, 1.0f));
	}

	if (m_fInvinDT > 0.6f)
	{
		SetObjStatus(OBJ_STATUS::NONE);
		m_fBeforeInvinDT = 0.f;
		m_fInvinDT = 0.f;

		SetObjColor(Vec4(1.f, 1.f, 1.f, 1.0f));
	}
}

void CMonster::Animation()
{
	m_fAnimDuration += DT;

	if (OBJ_TYPE::BOSS == GetObjType())
	{
		if (m_fAnimDuration > 0.1f)
		{
			if (m_iAnimIdx.x > 4)
			{
				m_iAnimIdx.x = 0;
				m_iAnimIdx.y += 1;
			}
			else
			{
				m_iAnimIdx.x += 1;
			}

			if (m_iAnimIdx.y > 1)
			{
				m_iAnimIdx.y = 1;
			}

			m_fAnimDuration = 0.f;
		}
	}
	else if (OBJ_TYPE::MONSTER == GetObjType())
	{
		if (m_fAnimDuration > 0.25f)
		{
			m_fAnimDuration = 0.f;
			m_iAnimIdx.x += 1;

			if (MONSTER_TYPE::GHOST == GetMonsterType())
			{
				if (m_iAnimIdx.x > 4)
				{
					m_iAnimIdx.x = 0;
				}
			}
			else if (MONSTER_TYPE::DEVIL == GetMonsterType())
			{
				if (m_iAnimIdx.x > 3)
				{
					m_iAnimIdx.x = 0;
				}
			}
		}

		if (m_iDir == 0)
		{
			m_iAnimIdx.y = 2;
		}
		else if (m_iDir == 1)
		{
			m_iAnimIdx.y = 3;
		}
		else if (m_iDir == 2)
		{
			m_iAnimIdx.y = 1;
		}
		else if (m_iDir == 3)
		{
			m_iAnimIdx.y = 0;
		}
	}
}

void CMonster::ChangeMonster()
{
	if (GetObjType() == OBJ_TYPE::MONSTER)
	{
		SetObjHp(BOSS_HP);
		SetObjType(OBJ_TYPE::BOSS);
		m_eBossStatus = BOSS_STATUS::PATTERN_SECOND;
	}
}

void CMonster::BossMove()
{
	Vec3 vPos = GetObjPos();
	Vec3 vSize = GetObjSize();

	Vec3 vForce = Vec3(0.f, 0.f, 0.f);
	float fJAmount = 50.f;// 5뉴튼 = 5*9.8 m/s2	

	m_fJumpCoolTime += DT;

	float term;
	if (m_eBossStatus == BOSS_STATUS::PATTERN_THIRD )
	{
		term = 2.f;
	}
	else if (m_eBossStatus == BOSS_STATUS::PATTERN_FOURTH)
	{
		term = 1.2f;
	}
	else
		term = 3.f;

	if (m_fJumpCoolTime > term)
	{
		m_fJumpCoolTime = 0.f;
		if (m_pPlayer != nullptr)
		{
			m_vPlayerPos = m_pPlayer->GetObjPos();
			m_vDist = m_vPlayerPos - vPos;
			m_vDir = m_vDist;
			m_vDir.Normalize();
		}
		m_bJump = true;
		vForce.z += 1.5f;
		vForce.x += m_vDist.x;
		vForce.y += m_vDist.y;

		if (m_eBossStatus == BOSS_STATUS::PATTERN_THIRD)
		{
			for (int i = 0; i < 1; ++i)
			{
			
				float vPosX = well512.GetFloatValue(-2, 2);
				float vPosY = well512.GetFloatValue(-2, 2);
				CScnMgr::GetInst()->AddObject(Vec3(vPosX, vPosY, 0.f), Vec3(0.25f, 0.25f, 0.3f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 10.f, 0.3f, OBJ_TYPE::MONSTER, this);

			}
		}

		int sound = CScnMgr::GetInst()->GetSound(8);
		Sound::GetInst()->PlayShortSound(sound, false, 1);
	}

	if (m_bJump)
	{
		
		if (m_eBossStatus != BOSS_STATUS::PATTERN_FIRST)
		{
			BossAttack();
		}
	
		float fSize = sqrt(vForce.x * vForce.x + vForce.y * vForce.y);
		if (fSize > FLT_EPSILON)
		{
			vForce.x /= fSize;
			vForce.y /= fSize;
			vForce.x *= fJAmount;
			vForce.y *= fJAmount;
		}

		if (vForce.z > FLT_EPSILON)
		{
			float fX = well512.GetFloatValue(5.f, 25.f);

			vForce.z *= fJAmount * 25.f;

			if (GetOwner() != nullptr)
			{
				vForce.x *= fX;
				vForce.y *= fX;
			}
			else
			{
				vForce.x *= 25.f;
				vForce.y *= 25.f;
			}

			if (m_vPos.z < FLT_EPSILON)
			{
				AddForce(Vec3(vForce.x, vForce.y, vForce.z), DT);
				SetObjPos(Vec3(vPos.x + m_vVel.x * DT, vPos.y + m_vVel.y*DT, vPos.z + m_vVel.z*DT));
				
				return;
			}
		}
	}

	if (vPos.x < -2.f) { vPos.x = -2.f; }
	if (vPos.x > 2.f) { vPos.x = 2.f; }
	if (vPos.y < -1.8f) { vPos.y = -1.8f; }
	if (vPos.y > 1.6f) { vPos.y = 1.6f; }

	//if (vPos.x > 2.5f - vSize.x || vPos.x < -2.5f + vSize.x)
	//	m_vVel.x *= -1;

	//if (vPos.y > 3.5f - vSize.y || vPos.y < -3.5f + vSize.y)
	//	m_vVel.y *= -1;

	//// 이 수식은 오 ㅐ들어 있지

	//float fAmount = 0.5f;// 5뉴튼 = 5*9.8 m/s2

	//if (m_vVel.x > 0.f)
	//{
	//	vForce.x -= fAmount;
	//}
	//else if (m_vVel.x < 0.f)
	//{
	//	vForce.x += fAmount;
	//}
	//if (m_vVel.y < 0.f)
	//{
	//	vForce.y += fAmount;
	//}
	//else if (m_vVel.y > 0.f)
	//{
	//	vForce.y -= fAmount;
	//}

	AddForce(vForce, DT);
	SetObjPos(Vec3(vPos.x + m_vVel.x * DT, vPos.y + m_vVel.y*DT, vPos.z + m_vVel.z*DT));
}

void CMonster::NormalMove()
{
	if (GetObjType() == OBJ_TYPE::MONSTER)
	{
		m_fDirTimer += DT;

		Vec3 vPos = GetObjPos();
		Vec3 vSize = GetObjSize();

		Vec3 vForce = Vec3(0.f, 0.f, 0.f);
		float fAmountX = 30.f; // 5뉴튼 = 5*9.8 m/s2	
		float fAmountY = 30.f; // 5뉴튼 = 5*9.8 m/s2	

		if (GetMonsterType() == MONSTER_TYPE::GHOST || GetObjType() == OBJ_TYPE::BOSS)
		{
			if (vPos.x < -2.f) { vPos.x = -2.f; }
			if (vPos.x > 2.f) { vPos.x = 2.f; }
			if (vPos.y < -1.8f) { vPos.y = -1.8f; }
			if (vPos.y > 1.2f) { vPos.y = 1.2f; }
		}
		else
		{
			if (vPos.x < -1.9f) { vPos.x = -1.9f; }
			if (vPos.x > 1.9f) { vPos.x = 1.9f; }
			if (vPos.y < -1.7f) { vPos.y = -1.7f; }
			if (vPos.y > 1.1f) { vPos.y = 1.1f; }
		}

		if (m_fDirTimer > 3.f)
		{
			m_iDir = well512.GetValue(0, 4);
			m_fDirTimer = 0.f;
		}

		if (m_iDir == 0)
		{
			vForce.x += fAmountX;
		}
		else if (m_iDir == 1)
		{
			vForce.x -= fAmountX;
		}
		else if (m_iDir == 2)
		{
			vForce.y += fAmountY;
		}
		else if (m_iDir == 3)
		{
			vForce.y -= fAmountY;
		}

	
		AddForce(vForce, DT);
		SetObjPos(Vec3(vPos.x + m_vVel.x * DT, vPos.y + m_vVel.y*DT, vPos.z + m_vVel.z*DT));
	}
}

void CMonster::BossAttack()
{
	SetForce(0.f);

	Vec3 vForce = Vec3(0.f, 0.f, 0.f);

	float fRandom = well512.GetFloatValue(-300.f, 300.f);
	float fMass = well512.GetFloatValue(1.f, 20.f);
	SetForce(fRandom);
	CScnMgr::GetInst()->AddObject(Vec3(m_vPos.x, m_vPos.y, m_vPos.z), Vec3(0.3f, 0.3f, 0.3f), Vec4(1.f, 0.f, 0.f, 1.f), Vec3(0.f, 0.f, 0.f), fMass, 0.3f, OBJ_TYPE::BULLET, this);
}

void CMonster::MonsterAttack()
{
	if (GetRemainingBulletCoolTime() <= 0.f)
	{
		Vec3 vForce = Vec3(0.f, 0.f, 0.f);

		SetForce(10.f);
		CScnMgr::GetInst()->AddObject(Vec3(m_vPos.x, m_vPos.y, m_vPos.z), Vec3(0.3f, 0.3f, 0.3f), Vec4(1.f, 0.f, 0.f, 1.f), Vec3(0.f, 0.f, 0.f), 10.f, 0.3f, OBJ_TYPE::BULLET, this);

		SetRemainingBulletCoolTime(2.f);
	}

	if (GetRemainingBulletCoolTime() >= 0.f)
		m_fRemainingBulletCoolTime -= DT;
}

bool CMonster::CanShootBullet()
{
	if (m_fRemainingBulletCoolTime < FLT_EPSILON)
	{
		return true;
	}
	return false;
}

void CMonster::ResetBulletCoolTime()
{
	m_fRemainingBulletCoolTime = m_fBulletCoolTime;
}

void CMonster::AddForce(Vec3 vForce, float fTime)
{
	Vec3 vAcc = vForce / m_fMass;

	m_vVel = m_vVel + vAcc * DT; // fTime 대신 DT 넣자

//	m_vAcc = Vec3(0.f, 0.f, 0.f);
}

