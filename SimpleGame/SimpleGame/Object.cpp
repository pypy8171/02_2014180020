#include "stdafx.h"
#include "Object.h"

#include "ScnMgr.h"
#include "TimeMgr.h"

CObject::CObject()
	: m_vPos(Vec3(0.f,0.f,0.f))
	, m_vSize(Vec3(0.f,0.f,0.f))
	, m_vColor(Vec4(1.f,1.f,1.f,1.f))
	, m_eObjType(OBJ_TYPE::NONE)
	, m_eLookDir(LOOK_DIR::NONE)
	, m_vVel(Vec3(0.f, 0.f, 0.f))
	, m_vAcc(Vec3(0.f, 0.f, 0.f))
	, m_vVol(Vec3(0.f, 0.f, 0.f))
	, m_fMass(0.f)
	, m_fAccTime(0.f)
	, m_fFricCoef(0.f)
	, m_fForce(0.f)
	, m_fHp(0.f)
	, m_fOffensePower(0.f)
	, m_fRemainingBulletCoolTime(0.f)
	, m_iTextureID(-1)
	, m_pOwner(nullptr)
	, m_iAnimIdx(Vec2(0, 0))
	, m_fAnimDuration(0.f)
	, m_bAnimEnd(false)
	, m_eObjStatus(OBJ_STATUS::NONE)
	, m_fInvinDT(0.f)
	, m_fBeforeInvinDT(0.f)
	, m_bJump(false)
{
}

CObject::~CObject()
{
}

int CObject::update()
{
	//Apply friction
	float fFn = (float)GRAVITY * m_fMass; // 수직항력
	float fFriction = m_fFricCoef * fFn; // 마찰력 

	float fDirX, fDirY, fDirZ;
	float fVelSize = std::sqrtf(m_vVel.x * m_vVel.x + m_vVel.y * m_vVel.y + m_vVel.z * m_vVel.z);
	   
	if (fVelSize > FLT_EPSILON)
	{
		fDirX = m_vVel.x / fVelSize;
		fDirY = m_vVel.y / fVelSize;
		fDirZ = m_vVel.z / fVelSize;

		// calculation friction force
		Vec3 vFriction = Vec3(-fDirX * fFriction, -fDirY * fFriction, 0.f);

		// calculate friction acc
		Vec3 vAcc = vFriction / m_fMass;
		vAcc.z = -GRAVITY;
		// update velocity by friction force
		Vec3 vNewVel = m_vVel + vAcc * DT;

		if (vNewVel.x * m_vVel.x < 0.f)
		{
			m_vVel.x = 0.f;
		}
		else
		{
			m_vVel.x = vNewVel.x;
		}

		if (vNewVel.y * m_vVel.y < 0.f)
		{
			m_vVel.y = 0.f;
		}
		else
		{
			m_vVel.y = vNewVel.y;
		}

		m_vVel.z = vNewVel.z;
	}

	else if (m_vPos.z > FLT_EPSILON)
	{
		float accz = -GRAVITY;
		float newvelz = m_vVel.z + accz * DT;
		m_vVel.z = newvelz;
	}
	else
	{
		m_vVel.x = 0.f;
		m_vVel.y = 0.f;
		m_vVel.z = 0.f;
	}

	//Update position
	m_vPos = m_vPos + m_vVel * DT;

	if (m_vPos.z < FLT_EPSILON)
	{
		m_vPos.z = 0.f;
		m_vVel.z = 0.f;
		if (m_bJump)
		{
			if(GetObjType() == OBJ_TYPE::BOSS)
				CScnMgr::GetInst()->SetCameraShaking(true);
			m_bJump = false;
		}
	}

	return 0;
}

bool CObject::IsAncestor(CObject * obj)
{
	if (obj != nullptr)
	{
		if (obj == m_pOwner)
		{
			return true;
		}
	}
	return false;
}



void CObject::AddGameObj(Vec3 vPos, Vec3 vSize, Vec4 vColor)
{
}

void CObject::DeleteGameObj(int iIdx)
{
}