#include "stdafx.h"
#include "Player.h"

#include "KeyMgr.h"
#include "ScnMgr.h"
#include "Renderer.h"

#include "TimeMgr.h"
#include "Sound.h"
#include "Life.h"

#include "Dependencies\freeglut.h"
// 바라보는 방향 설정해야함.

CPlayer::CPlayer()
	: m_bBulletFull(false)
	, m_vDir(Vec3(0.f, 0.f, 0.f))
	, m_vPrevPos(Vec3(0.f, 0.f, 0.f))
	, m_iLife(5)
	, m_vBodyIdx(Vec2(0,0))
	, m_vHeadIdx(Vec2(0, 0))
	, m_bHeadChange(false)
	, m_iMoveAnim(-1)
{
	init();
}

CPlayer::~CPlayer()
{
}

void CPlayer::init()
{
	SetObjType(OBJ_TYPE::PLAYER);
	SetLookDir(LOOK_DIR::LEFT);
	m_vVel = Vec3(0.f, 0.f, 0.f);
}

int CPlayer::update()
{
	CObject::update();

	m_vPrevPos = GetObjPos();

	m_fAccTime += DT;

	Status();
	InputKey();

	m_vDir = (GetObjPos() - m_vPrevPos);
	m_vDir.Normalize();

	return 0;
}

void CPlayer::InputKey()
{
	DirKey();

	SetForce(0.f);

	Vec2 vHeadIdx = GetHeadIdx();

	if ((KEYTAB(KEY_TYPE::KEY_LEFT)|| KEYTAB(KEY_TYPE::KEY_RIGHT) || KEYTAB(KEY_TYPE::KEY_UP) || KEYTAB(KEY_TYPE::KEY_DOWN) ))
	{
		if (KEYTAB(KEY_TYPE::KEY_LEFT)) { SetLookDir(LOOK_DIR::LEFT); vHeadIdx.x = 6; }
		else if (KEYTAB(KEY_TYPE::KEY_RIGHT)) { SetLookDir(LOOK_DIR::RIGHT); vHeadIdx.x = 2;	}
		else if (KEYTAB(KEY_TYPE::KEY_UP)) { SetLookDir(LOOK_DIR::UP); vHeadIdx.x = 4;	}
		else if (KEYTAB(KEY_TYPE::KEY_DOWN)) { SetLookDir(LOOK_DIR::DOWN); vHeadIdx.x = 0;	}

		if (GetRemainingBulletCoolTime() <= 0.f && !m_bHeadChange)
		{
			m_bHeadChange = true;

			vHeadIdx.x += 1;

			SetForce(2000.f);
			CScnMgr::GetInst()->AddObject(Vec3(m_vPos.x, m_vPos.y, m_vPos.z), Vec3(0.5f, 0.5f, 0.f), Vec4(1.f, 1.f, 1.f, 1.f), Vec3(0.f, 0.f, 0.f), 10.f, 0.3f, OBJ_TYPE::BULLET, this);
			SetRemainingBulletCoolTime(0.25f);

			int sound = CScnMgr::GetInst()->GetSound(5);
			Sound::GetInst()->PlayShortSound(sound, false, 2.f);
		}
	}
	else if ((KEYAWAY(KEY_TYPE::KEY_LEFT) || KEYAWAY(KEY_TYPE::KEY_RIGHT) || KEYAWAY(KEY_TYPE::KEY_UP) || KEYAWAY(KEY_TYPE::KEY_DOWN)) && m_bHeadChange)
	{
		m_bHeadChange = false;
		vHeadIdx.x -= 1;
	}

	if(GetRemainingBulletCoolTime()>=0.f)
		m_fRemainingBulletCoolTime -= DT;

	SetHeadIdx(vHeadIdx);
}

void CPlayer::DirKey()
{
	Vec3 vForce = Vec3(0.f, 0.f, 0.f);
	float fAmount = 50.f;// 5뉴튼 = 5*9.8 m/s2	

	Vec2 vBodyIdx = GetBodyIdx();

	if (vBodyIdx.x > 8)
	{
		vBodyIdx.x = 0;
	}

	if (KEYHOLD(KEY_TYPE::KEY_A)) { 
		vForce.x -= fAmount; 
		cout << m_vVel.x << endl;
		vBodyIdx.x += 1; 
		vBodyIdx.y = 4; }
	else if (KEYHOLD(KEY_TYPE::KEY_D)){ vForce.x += fAmount; vBodyIdx.x += 1; vBodyIdx.y = 3;}
	else if (KEYHOLD(KEY_TYPE::KEY_W)){ vForce.y += fAmount; vBodyIdx.x += 1; vBodyIdx.y = 2;}
	else if (KEYHOLD(KEY_TYPE::KEY_S)) { vForce.y -= fAmount; vBodyIdx.x += 1; vBodyIdx.y = 2;}
	else{vBodyIdx.x += 1;vBodyIdx.y = 1;}

	if (m_vVel.x > 1.5f)
		m_vVel.x = 1.5f;
	if (m_vVel.x < -1.5f)
		m_vVel.x = -1.5f;
	if (m_vVel.y > 1.5f)
		m_vVel.y = 1.5f;
	if (m_vVel.y < -1.5f)
		m_vVel.y = -1.5f;

	//if (vForce.x > 10)
	//	vForce.x = 10;
	//if (vForce.y > 10)
	//	vForce.y = 10;

	if (m_vPos.x < -2.f){m_vPos.x = -2.f;}
	if (m_vPos.x > 2.f) { m_vPos.x = 2.f; }
	if (m_vPos.y < -1.8f) { m_vPos.y = -1.8f; }
	if (m_vPos.y > 1.2f) { m_vPos.y = 1.2f; }

	if (KEYTAB(KEY_TYPE::KEY_CTRL) && !m_bJump)
	{
		m_bJump = true;
		vForce.z += 1.f;
	}

	SetBodyIdx(vBodyIdx);

	float fSize = sqrt(vForce.x * vForce.x + vForce.y * vForce.y);
	if (fSize > FLT_EPSILON)
	{
		vForce.x /= fSize;
		vForce.y /= fSize;
		vForce.x *= fAmount;
		vForce.y *= fAmount;
		
	}

	if (vForce.z > FLT_EPSILON)
	{
		vForce.z *= fAmount * 25.f;

		if (m_vPos.z < FLT_EPSILON)
		{
			AddForce(Vec3(0.f, 0.f, vForce.z), DT);
			return;
		}
	}

	AddForce(vForce, DT);
	SetObjPos(Vec3(m_vPos.x + m_vVel.x*DT, m_vPos.y + m_vVel.y*DT, m_vPos.z + m_vVel.z*DT));
}

void CPlayer::Status()
{
	if (GetObjStatus() == OBJ_STATUS::INVINCIBILITY)
	{
		m_fInvinDT += DT;

		if (m_fInvinDT - m_fBeforeInvinDT > 0.05f)
			m_fBeforeInvinDT = m_fInvinDT;

		if (m_fBeforeInvinDT == m_fInvinDT)
			SetObjColor(Vec4(0.f, 0.f, 0.f, 0.f));
		else if (m_fBeforeInvinDT != m_fInvinDT)
			SetObjColor(Vec4(1.f, 1.f, 1.f, 1.0f));
	}

	if (m_fInvinDT > 1.f)
	{
		SetObjStatus(OBJ_STATUS::NONE);
		m_fBeforeInvinDT = 0.f;
		m_fInvinDT = 0.f;

		SetObjColor(Vec4(1.f, 1.f, 1.f, 1.0f));
	}
}

bool CPlayer::CanShootBullet()
{
	if (m_fRemainingBulletCoolTime < FLT_EPSILON)
	{
		return true;
	}
	return false;
}

void CPlayer::ResetBulletCoolTime()
{
	m_fRemainingBulletCoolTime = m_fBulletCoolTime;
}

void CPlayer::AddForce(Vec3 vForce, float fTime)
{
	Vec3 vAcc = vForce / m_fMass;

	m_vVel = m_vVel + vAcc * DT; // fTime 대신 DT 넣자


}

// 이 수식으로는 가만히 있을때 흔들림
//if (m_vVel.x > 0.f)
//{
//	vForce.x -= fAmount;
//}
//else if (m_vVel.x < 0.f)
//{
//	vForce.x += fAmount;
//}
//if (m_vVel.y < 0.f)
//{
//	vForce.y += fAmount;
//}
//else if (m_vVel.y > 0.f)
//{
//	vForce.y -= fAmount;
//}