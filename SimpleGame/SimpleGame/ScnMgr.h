#pragma once
#include "global.h"

#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include "Dependencies\glew.h"

class Sound;
class CObject;
class CPlayer;
//class CBullet;
//class CMonster;
class Renderer;

class CScnMgr
{
	SINGLE(CScnMgr);

private:
	Renderer*		m_pRenderer;

	Sound*			m_pSound;

	CObject*		m_pPlayer;
	CObject*		m_pMonster[MAX_MONSTERS];
	CObject*		m_pBullet[MAX_BULLETS];
	CObject*		m_pMap;
	CObject*		m_pLife[MAX_LIFE];
	CObject*		m_pDoor[MAX_DOOR];
	CObject*		m_pUI;
	CObject*		m_pLogo;

	int				m_iSound[30];
	int				m_iTexture[30];

	int				m_iTestIdx = -1;

	bool			m_bKeyW = false;
	bool			m_bKeyA = false;
	bool			m_bKeyS = false;
	bool			m_bKeyD = false;

	bool			m_bKeyUp = false;
	bool			m_bKeyDown = false;
	bool			m_bKeyLeft = false;
	bool			m_bKeyRight = false;

	bool			m_bParticle;
	int				m_iParticleTexture[MAX_PARTICLE];
	int				m_iParticleObj[MAX_PARTICLE];
	float			m_fParticleDuration[MAX_PARTICLE];
	bool			m_bParticleOn[MAX_PARTICLE];
	Vec3			m_vParticlePos[MAX_PARTICLE];
	bool			m_bExit;

	MAP				m_eCurMap;
	MAP				m_eBeforeMap; 
	bool			m_bLogo;
	bool			m_bGameStart;
	bool			m_bGameEnd;

	float			m_fResetTimer;
	bool			m_bResetOn;

	bool			m_bCameraShake;
	float			m_fShakingTimer;
	Vec2			m_vCamPos;

public:
	int update(float eTime);
	void init();
	void initobj();
	void RenderScene();
	
	const bool& GetExit() { return m_bExit; }

public:
	// pObject 는 addobject를 호출한 객체 ex) 플레이어와 총알
	int	AddObject(Vec3 vPos, Vec3 vSize, Vec4 vColor, Vec3 vVel, float mass, float fricCoef, OBJ_TYPE eObjType, CObject* pObject = nullptr); 
	void DeleteObject(int idx, CObject* pObject);

	void DeleteScene();
	void DoGarbageCollection();

	int& GetSound(int i) { return m_iSound[i]; }

	const bool& GetGameStart() { return m_bGameStart; }
	const bool& GetGameEnd() { return m_bGameEnd; }

	void SetCameraShaking(bool _b) { m_bCameraShake = _b; }
	void CemeraShaking();

public:
	void KeyDownInput(unsigned char key, int x, int y); // 마우스 위치
	void KeyUpInput(unsigned char key, int x, int y);
	void SpecialKeyDownInput(int key, int x, int y);
	void SpecialKeyUpInput(int key, int x, int y);

public:
	void Collision();

public:
	void SetRenderer(Renderer* pRenderer) { m_pRenderer = pRenderer; }
	Renderer* GetRenderer() { return m_pRenderer; }

	// 콜백 때문에 복사 생성자 만듬 (소멸자 호출하기 위함)
public:
	CScnMgr(const CScnMgr& _other);

};

