#pragma once
#include "SimpleMath.h"

#define SINGLE(className) public: \
						static className* GetInst() \
						{\
							static className mgr;\
							return &mgr;\
						}\
						public:\
							className();\
							~className();

//#define SINGLE(className) private:\
//	static className*	m_pThis; \
//public:\
//	static className* GetInst()\
//	{\
//		if (nullptr == m_pThis)\
//			m_pThis = new className;\
//		return m_pThis;\
//	}\
//	static void Destroy()\
//	{\
//		if (NULL != m_pThis) { delete m_pThis; m_pThis = nullptr; };\
//	}\
//public:\
//	className();\
//	~className();\

#define HERO_ID 0

#define GRAVITY 9.8

#define EXIT_MAINLOOP 	if (KEYTAB(KEY_TYPE::KEY_ESC))\
	{\
	DeleteScene();\
\
	glutLeaveMainLoop();\
	glutDestroyWindow(0);\
	}\

#define SAFE_DELETE(t) 	delete t; t = NULL;

#define MAX_OBJECTS 1000
#define MAX_BULLETS 1000
#define MAX_MONSTERS 20
#define MAX_LIFE 5
#define BOSS_HP 200
#define MAX_DOOR 4
#define MAX_PARTICLE 6
#define ROOM_MONSTER 8

#define KEYHOLD(type) CKeyMgr::GetInst()->GetKeyState(type, KEY_STATE::HOLD)
#define KEYAWAY(type) CKeyMgr::GetInst()->GetKeyState(type, KEY_STATE::AWAY)
#define KEYTAB(type) CKeyMgr::GetInst()->GetKeyState(type, KEY_STATE::TAB)

#define DT CTimeMgr::GetInst()->DeltaTime()
#define PI 3.141592;
#define RAD PI/180;

typedef DirectX::SimpleMath::Vector2 Vec2;
typedef DirectX::SimpleMath::Vector3 Vec3;
typedef DirectX::SimpleMath::Vector4 Vec4;

using namespace DirectX::SimpleMath;



enum class OBJ_TYPE
{
	NONE,
	PLAYER,
	BOSS,
	MONSTER,
	BULLET,
	LIFE,
	UI,
	LOGO,
	MAP,
	DOOR,
	END
};

enum class SOUND
{
	TEARSBLOB,
	TEARSFIRE,
	TEARSBREAK,
};

enum class MAP
{
	START,
	ROOM1,
	ROOM2,
	ROOM3,
	BOSSROOM,
};

enum class OBJ_STATUS
{
	NONE,
	INVINCIBILITY,
	ATTACK,
	DEFENSE,
	JUMP,
	END,
};

enum class BOSS_STATUS
{
	NONE,
	PATTERN_FIRST,
	PATTERN_SECOND,
	PATTERN_THIRD,
	PATTERN_FOURTH,
	END,
};

enum class MONSTER_TYPE
{
	NONE,
	GHOST,
	DEVIL,
	BOSS,
	END,
};

enum class BULLET_TYPE
{
	NONE,
	NORMAL,
	TARGETED,
	GUIDED,
	MULTI,
	BOMB,
	END,
};

enum class ITEM_TYPE
{
	NONE,
	END,
};

enum class LOOK_DIR
{
	NONE,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

enum class KEY_TYPE
{
	KEY_1 = 0,
	KEY_2,
	KEY_3,
	KEY_4,
	KEY_5,
	KEY_6,
	KEY_7,
	KEY_8,
	KEY_9,
	KEY_0,
	KEY_Q,
	KEY_W,
	KEY_E,
	KEY_R,
	KEY_T,
	KEY_Y,
	KEY_U,
	KEY_I,
	KEY_O,
	KEY_P,
	KEY_A,
	KEY_S,
	KEY_D,
	KEY_F,
	KEY_G,
	KEY_H,
	KEY_J,
	KEY_K,
	KEY_L,
	KEY_Z,
	KEY_X,
	KEY_C,
	KEY_V,
	KEY_B,
	KEY_N,
	KEY_M,
	KEY_F1,
	KEY_F2,
	KEY_F3,
	KEY_F4,
	KEY_F5,
	KEY_F6,
	KEY_F7,
	KEY_F8,
	KEY_F9,
	KEY_F10,
	KEY_F11,
	KEY_F12,

	KEY_CTRL,
	KEY_ALT,
	KEY_LSHIFT,
	KEY_RSHIFT,
	KEY_TAB,
	KEY_CAP,
	KEY_ENTER,
	KEY_ESC,
	KEY_SPACE,
	KEY_UP,
	KEY_DOWN,
	KEY_LEFT,
	KEY_RIGHT,

	KEY_NUM1,
	KEY_NUM2,
	KEY_NUM3,
	KEY_NUM4,
	KEY_NUM5,
	KEY_NUM6,
	KEY_NUM7,
	KEY_NUM8,
	KEY_NUM9,
	KEY_NUM0,

	KEY_LBTN,
	KEY_RBTN,

	END,
};

enum class KEY_STATE
{
	TAB,	// 최초 눌린 시점
	HOLD,   // 누르고 있는 중
	AWAY,   // 키를 막 뗀 시점
	NONE,   // 안눌림	
};