#pragma once


//struct Vec2
//{
//	float x;
//	float y;
//
//public:
//	float Distance(const Vec2& _vOther) const
//	{
//		return sqrt(pow(x - _vOther.x, 2) + pow(y - _vOther.y, 2));
//	}
//
//	float Inclination(const Vec2& _vOther) const
//	{
//		return (x - _vOther.x) / (y - _vOther.y);
//	}
//
//public:
//	void Normalize()
//	{
//		float fDist = sqrt(x*x + y * y);
//		if (fDist == 0.f)
//			assert(NULL);
//
//		x /= fDist;
//		y /= fDist;
//	}
//
//	void Inner(float _fAngle)
//	{
//		float fInner = sqrt(x*x + y * y) * cos(_fAngle);
//	}
//
//	void operator=(const POINT& _pt)
//	{
//		x = (float)_pt.x;
//		y = (float)_pt.y;
//	}
//
//	Vec2 operator/(float _f)
//	{
//		if (_f == 0)
//			assert(NULL);
//
//		return Vec2(x / _f, y / _f);
//	}
//
//	Vec2 operator/(const Vec2& _vecOther)
//	{
//		if (_vecOther.x == 0 || _vecOther.y == 0)
//			assert(NULL);
//
//		return Vec2(x / _vecOther.x, y / _vecOther.y);
//	}
//
//	Vec2& operator /=(const Vec2& _vecOther)
//	{
//		if (_vecOther.x == 0 || _vecOther.y == 0)
//			assert(NULL);
//
//		x /= _vecOther.x;
//		y /= _vecOther.y;
//
//		return *this;
//	}
//
//	Vec2 operator-(const Vec2& _vecOther) const //  이맴버 함수안에서는 맴버를 절대로 수정하지 않겠다 -> // 1.15 이해 필요
//	{
//		return Vec2(x - _vecOther.x, y - _vecOther.y);
//	}
//
//	Vec2 operator-(const Vec2& _vecOther)
//	{
//		return Vec2(x - _vecOther.x, y - _vecOther.y);
//	}
//
//	Vec2 operator*(const Vec2& _vecOther)
//	{
//		return Vec2(x * _vecOther.x, y*_vecOther.y);
//	}
//
//	Vec2 operator*(const float& _fFloat)
//	{
//		return Vec2(x * _fFloat, y*_fFloat);
//	}
//
//	Vec2& operator-=(const Vec2& _vecOther)
//	{
//		x -= _vecOther.x;
//		y -= _vecOther.y;
//
//		return *this;
//	}
//
//	Vec2 operator+(const Vec2& _vecOther)
//	{
//		return Vec2(x + _vecOther.x, y + _vecOther.y);
//	}
//
//	Vec2 operator+(const Vec2& _vecOther) const
//	{
//		return Vec2(x + _vecOther.x, y + _vecOther.y);
//	}
//
//	Vec2& operator+=(const Vec2& _vecOther)
//	{
//		x += _vecOther.x;
//		y += _vecOther.y;
//
//		return *this;
//	}
//
//	Vec2()
//		: x(0.f)
//		, y(0.f)
//	{}
//
//	Vec2(float _x, float _y)
//		: x(_x)
//		, y(_y)
//	{}
//
//	Vec2(const POINT& _pt)
//		: x((float)_pt.x)
//		, y((float)_pt.y)
//	{}
//
//};