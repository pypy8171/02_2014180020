#pragma once
#include "Object.h"

class CPlayer;

class CBullet :
	public CObject
{
private:
	CObject*	m_pPlayer;
	BULLET_TYPE m_eBulletType;

	Vec3		m_vDir;
	bool		m_bKnowOwner;
	bool		m_bShooted;

	int			m_iBounceCount;
	bool		m_bCollided;


public:
	virtual int update();
	virtual void init();

public:
	void SetBulletType(BULLET_TYPE _eType) { m_eBulletType = _eType; }
	const BULLET_TYPE& GetBulletType() { return m_eBulletType; }

	void SetPlayer(CObject* pObject) { m_pPlayer = pObject; }
	const CObject* GetPlayer() { return m_pPlayer; }

	void SetBulletShooted(bool _bool) { m_bShooted = _bool; }
	const bool& GetBulletShooted() { return m_bShooted; }

	void SetBounceCount() { m_iBounceCount += 1; }
	const int& GetBounceCount() { return m_iBounceCount; }

public:
	void SetCollided(bool _col) { m_bCollided = _col; }
	const bool& GetCollided() { return m_bCollided; }

public:
	virtual void AddForce(Vec3 vForce, float fTime);

public:
	void Shoot();

public:
	CBullet();
	virtual ~CBullet();
};

