/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"

#include "Renderer.h"

#include "KeyMgr.h"
#include "ScnMgr.h"
#include "TimeMgr.h"

int g_prevTimeInMillisecond = 0;

void RenderScene(int eTime)
{
	int iCurrentTime = glutGet(GLUT_ELAPSED_TIME);
	int elapsedTime = iCurrentTime - g_prevTimeInMillisecond;

	g_prevTimeInMillisecond = iCurrentTime;

	// cout << elapsedTime << endl;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// Renderer Test
	CScnMgr::GetInst()->update((float)eTime);
	if (!CScnMgr::GetInst()->GetExit())
	{
		CScnMgr::GetInst()->RenderScene(); // elapsedtime 사용하려면 파라미터로 넘기면 됨.
		CScnMgr::GetInst()->DoGarbageCollection();
	}

	glutSwapBuffers();

	glutTimerFunc(16, RenderScene, 16);
}

void Idle(void)
{
	// RenderScene();
}

void MouseInput(int button, int state, int x, int y)
{
	// RenderScene();
}

void KeyDownInput(unsigned char key, int x, int y)
{
	// RenderScene();
	CKeyMgr::GetInst()->SetMousePos(x, y);
	CScnMgr::GetInst()->KeyDownInput(key, x, y);
	CScnMgr::GetInst()->SpecialKeyDownInput(key, x, y);
}

void KeyUpInput(unsigned char key, int x, int y)
{
	// RenderScene();
	CKeyMgr::GetInst()->SetMousePos(x, y);
	CScnMgr::GetInst()->KeyUpInput(key, x, y);
}

void SpecialKeyDownInput(int key, int x, int y)
{
	// RenderScene();
	CScnMgr::GetInst()->SpecialKeyDownInput(key, x, y);
}

void SpecialKeyUpInput(int key, int x, int y)
{
	// RenderScene();
	CScnMgr::GetInst()->SpecialKeyUpInput(key, x, y);
}

int main(int argc, char **argv)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(253);

	// Initialize GL things

	CKeyMgr::GetInst()->init();
	CTimeMgr::GetInst()->init();

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1000, 1000);
	glutCreateWindow("Game Software Engineering KPU");

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}

	// Initialize Renderer
	CScnMgr::GetInst()->SetRenderer(new Renderer(500,500));

	if (!CScnMgr::GetInst()->GetRenderer()->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	glutDisplayFunc(Idle);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardUpFunc(KeyUpInput);
	glutMouseFunc(MouseInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);

	g_prevTimeInMillisecond = glutGet(GLUT_ELAPSED_TIME);

	glutTimerFunc(16, RenderScene, 16);

	glutMainLoop();

    return 0;
}

