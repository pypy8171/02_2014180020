#pragma once

#include "global.h"

class CObject;

class CPhysics
{
	SINGLE(CPhysics);

private:
	bool	BBcollision(CObject* pFirstObject, CObject* pSecondObject);

public:
	bool IsCollision(CObject* pFirstObject, CObject* pSecondObject, int iType);
	void ProcessCollision(CObject* apFirstObject, CObject* pSecondObject);

};

